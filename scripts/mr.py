#!/usr/bin/env python3

import os
import subprocess
import argparse

os.putenv("SYMFONY_ENV", "mr")
# **kwargs just takes whatever keyword argument is given and shoves it in the kwargs dictionary.
def exec_symfony_cmd(cmd, **kwargs):
    # check_call raises an exception if the return value is not 0
    # This behaviour is exactly what we want here.
    subprocess.check_call(["php", "app/console", cmd] +
        # The [ ] line is a list comprehension. It generates a list on the fly by appending
        # all the results of the expression before the "for" keyword applied to the elements of
        # the iterable after the "in" keyword. It is quite handy, really.
        ['--{}'.format(k) if v is None else '--{}={}'.format(k, v) for k,v in kwargs.items()]
    )

parser = argparse.ArgumentParser(description="A script that launches a session set to use the MR environment")
hard_reset = parser.add_mutually_exclusive_group()
hard_reset.add_argument("--clean", '-c', action="store_true",
                        help="Nuke the whole MR database and recreate it with a new user"
                       )
hard_reset.add_argument("--no-clean", action="store_true")

compile_assets = parser.add_mutually_exclusive_group()
compile_assets.add_argument("--regen",  action="store_true",
                        help="Recompiles the assets (default)"
                       )
compile_assets.add_argument("--no-regen", '-n', action="store_true")

result = parser.parse_args()

if result.clean:
    try:
        os.remove('src/LLDC/Bundle/Resources/config/lldc.yml')
    except OSError:
        pass
    import yaml
    f = open('src/LLDC/Bundle/Resources/config/lldc.yml', 'w')
    f.write(yaml.dump({"parameters":{"lldc":{}}}))
    f.close()
    try:
        exec_symfony_cmd("doctrine:database:drop", force=None)
    except subprocess.CalledProcessError:
        # pass means "Do Nothing."
        pass
    exec_symfony_cmd("doctrine:database:create")
    exec_symfony_cmd("doctrine:schema:create")
    exec_symfony_cmd("lldc:generate:config")
    exec_symfony_cmd("lldc:generate:config", enrichment=None)
    exec_symfony_cmd("lldc:init-db")
    exec_symfony_cmd("lldc:admin:user:create", username="test", realmname="test", email="test@test.com", password="test")

if not result.no_regen:
    exec_symfony_cmd("assets:install")
    exec_symfony_cmd("assetic:dump")

print('''

    The environment has been successfully bootstrapped.
    Now spawning a new shell.
    Type \"exit\" or press Ctrl-D to come back to your
    previous shell session.

''')

os.execv(os.getenv("SHELL"), [os.getenv("SHELL")])
