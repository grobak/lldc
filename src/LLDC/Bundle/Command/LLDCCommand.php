<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This abstract class, extended by all commands, provides useful shortcuts
 */
abstract class LLDCCommand extends ContainerAwareCommand
{
    /**
     * Gets the repository for a class.
     * @param string $className
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository($className) {
        return $this->getContainer()->get('doctrine')->getRepository($className);
    }

    /**
     * Returns the entity manager
     * @return \Symfony\Bundle\DoctrineBundle\Registry The Doctrine Registry service
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Returns the LLDC's configuration
     * @return array(mixed) The LLDC's configuration
     */
    protected function getLLDC() {
        return $this->getContainer()->getParameter('lldc');
    }

    /**
     * Output a message indicating that the command ended properly
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function end($output) {
        $output->writeln('<info>-- The command ended properly --</info>');
    }
}
