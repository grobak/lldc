<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * This command allows you to display a battlefield.
 * * Usage : <b>php app/console lldc:realm:battlefield:display -b X</b>
 */
class BattlefieldDisplayCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:battlefield:display')
            ->setDescription('Utility command to display a battlefield.')
            ->setHelp('This command allows you to display a battlefield.')
            ->addOption('battlefield-id', 'b', InputOption::VALUE_REQUIRED, "Battlefield id")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Fetching parameters
        $lldc = $this->getLLDC();

        $bfId = $input->getOption('battlefield-id');

		if(empty($bfId)) {
            $output->writeln("<info>".$this->getSynopsis()."</info>");
            return;
		}

        $bf = $this->getRepository('LLDCBundle:Battlefield')->findOneById($bfId);
        $result = $this->getRepository('LLDCBundle:BattlefieldSquare')->findBy(array('battlefield' => $bf));
        $rows = array();

        $min_i = 0;
        $min_j = 0;
        $max_i = 0;
        $max_j = 0;
        foreach ($result as $s) {
            $i = $s->getOffsetR();
            $min_i = min($min_i, $i);
            $max_i = max($max_i, $i);
            $j = $s->getOffsetQ();
            $min_j = min($min_j, $j);
            $max_j = max($max_j, $j);
            if (!isSet($rows[$i]))
                $rows[$i] = array();
            $rows[$i][$j] = $s;
        }

        $lineCount = 0;
        for ($i = $min_i; $i <= $max_i; $i++) {
            $row = $rows[$i];
            if ($i % 2 == 0) {
                $line = "";
                for ($j = $min_j; $j <= $max_j; $j++) {
                    if (isSet($row[$j]))
                        $line .= " /\\ ";
                    else
                        $line .= "    ";
                }
                $line .= "\n";

                for ($j = $min_j; $j <= $max_j; $j++) {
                    if (isSet($row[$j]))
                        $line .= "/  \\";
                    else
                        $line .= "    ";
                }
                $line .= "\n";

                for ($j = $min_j; $j <= $max_j; $j++) {
                    if (isSet($row[$j])) {
                        $gt = $row[$j]->GetGroundType();
                        if ($gt == "grass")
                            $line .= '|<info>'.$gt[0].$gt[1].'</info>|';
                        else
                            $line .= '|<comment>'.$gt[0].$gt[1].'</comment>|';
                    } else
                        $line .= "    ";
                }
                $line .= "\n";

                for ($j = $min_j; $j <= $max_j; $j++) {
                    if (isSet($row[$j]))
                        $line .= "\\  /";
                    else
                        $line .= "    ";
                }
                $line .= "\n";

                for ($j = $min_j; $j <= $max_j; $j++) {
                    if (isSet($row[$j]))
                        $line .= " \\/ ";
                    else
                        $line .= "    ";
                }
                $line .= "\n";
                $output->write($line);
            } else {
                $line = "  ";
                for ($j = $min_j; $j <= $max_j; $j++) {
                    if (isSet($row[$j])) {
                        $gt = $row[$j]->GetGroundType();
                        if ($gt == "grass")
                            $line .= '|<info>'.$gt[0].$gt[1].'</info>|';
                        else
                            $line .= '|<comment>'.$gt[0].$gt[1].'</comment>|';
                    } else
                        $line .= "    ";
                }
                $output->writeln($line);
            }
        }
    $this->end($output);
    }
}
