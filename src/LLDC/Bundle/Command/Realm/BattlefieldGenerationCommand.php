<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * This command generates a battlefield for a given realm.
 * * Usage : <b>php app/console lldc:realm:battlefield:generate</b>
 */
class BattlefieldGenerationCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:battlefield:generate')
            ->setDescription('Utility command to generate a battlefield for a realm.')
            ->setHelp('This command allows you to generate a battlefield.')
            ->addOption('realm-id', 'r', InputOption::VALUE_REQUIRED, "Realm id")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Fetching parameters
        $lldc = $this->getLLDC();

        $realmId = $input->getOption('realm-id');

		if(empty($realmId)) {
            $output->writeln("<info>".$this->getSynopsis()."</info>");
            return;
		}

        $realm = $this->getRepository('LLDCBundle:Realm')->findOneById($realmId);

        if(is_null($realm)) {
            $output->writeln("<error>The realm n°".$realmId." doesn't exist.</error>");
            return;
        }

        $service = $this->getContainer()->get('lldc.battlefield');
        $bf = $service->addBattlefield($realm);
        if ($bf) {
            $this->getManager()->flush();

            $output->writeln("<info>Battlefield ID: ".$bf->getId()."</info>");
        } else {
            $output->writeln("<info>All battlefields for this realm have been generated.</info>");
        }

        $this->end($output);
    }
}
