<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Command\Realm
 */
namespace LLDC\Bundle\Command\Realm;

use LLDC\Bundle\Command\LLDCCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use LLDC\Bundle\Entity\Notification;
use LLDC\Bundle\Entity\RealmBuilding;

/**
 * This command updates ongoing buildings.
 * * Usage : <b>php app/console lldc:realm:ongoing:buildings</b>
 */
class OngoingBuildingsCommand extends LLDCCommand
{
    protected function configure()
    {
        $this
            ->setName('lldc:realm:ongoing:buildings')
            ->setDescription('Updates ongoing buildings for all actives realms')
            ->setHelp('This command updates ongoing buildings for every players.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getManager();

        // Fetching parameters
        $lldc = $this->getLLDC();

        // Fetching the games to initialize realm settings
        $games = $this->getRepository('LLDCBundle:Game')->findAll();

        $now = new \DateTime();

        foreach($games as $game) {
            $output->write('<comment>'.$game->getLabel()." [ratio=".$game->getBuildingsSpeed()."]:</comment>\n");

            $realms = $this->getRepository('LLDCBundle:Realm')->findByGame($game->getId());
            foreach($realms as $realm) {
                $output->writeln('<info>'.$realm->getPlace()->getName().' :</info>');

                $production = $realm->getProduction()->getBuildingProdPercent();
                $peonTotal = $this->getRepository('LLDCBundle:Troop')->findSumAvailableByRealmAndTypeAsTotal($realm, 'peon')['total'];
                $peonAvailable = floor($peonTotal * ($production/100));
                $output->writeln("\tpeon production on buildings : ".$production."% (".$peonTotal."x".$production."% : ".$peonAvailable." peon)");

                $buildings = $realm->getOngoingBuilding();
                if(!is_null($buildings)) {
                    // We get the ratio for this realm (using the race and the game we're on)
                    $ratio = $lldc['races'][$realm->getCharacter()->getRace()->getLabel()]['buildings']['speed'] * $game->getBuildingsSpeed();
                    foreach($buildings as $building) {
                        $buildingConf = $lldc['buildings'][$realm->getCharacter()->getRace()->getLabel()][$building->getType()];
                        if($peonAvailable==0) {
                            $output->writeln("\t\t<comment>No more peon available for the next buildings.</comment>");
                            break;
                        }

                        // TODO include the race and the game configurations
                        $totalDuration = $buildingConf['cost']['delay'] * $ratio;

                        // If this is the first time we work on this building, we set the date of last work to now
                        if($building->getDateBegin()->getTimestamp() == $building->getDateLastWork()->getTimestamp()) {
                            $building->setDateLastWork($now);
                        }
                        $output->writeln("\t\t<info>1 ".$building->getType()." [total=".$totalDuration."s]</info>\n\t\t".$buildingConf['cost']['peon']." peon can be on it.");
                        // We define how many peon work on this building
                        if($buildingConf['cost']['peon'] >= $peonAvailable) {
                            $peonOnThisOne = $peonAvailable;
                            $peonAvailable = 0;
                        }
                        else {
                            $peonOnThisOne = $buildingConf['cost']['peon'];
                            $peonAvailable -= $peonOnThisOne;
                        }

                        $output->writeln("\t\t".$peonOnThisOne." work on this one.");

                        // Seconds elapsed
                        $secondsElapsed = $now->getTimestamp() - $building->getDateBegin()->getTimestamp();
                        // Time elapsed since last work on this building
                        $timeElapsedSinceLastWork = $now->getTimestamp() - $building->getDateLastWork()->getTimestamp();
                        // New duration
                        $newDuration = $building->getDuration() - $timeElapsedSinceLastWork*$peonOnThisOne;

                        $estimatedEnd = new \DateTime();
                        $estimatedEnd->setTimestamp($now->getTimestamp() + floor($newDuration/$peonOnThisOne));

                        $output->writeln("\t\t".($newDuration / $peonOnThisOne)." seconds remaining (".$secondsElapsed." seconds elapsed since the beggining, ".$timeElapsedSinceLastWork." since last work).");

                        // Update the new duration left (amount of seconds for one villager), and the date of the last work (now)
                        $building->setDuration($newDuration);
                        $building->setDateLastWork($now);
                        $building->setDateEstimatedEnd($estimatedEnd);

                        // We calculate the global progression (%)
                        $progression = (($totalDuration - $newDuration) / $totalDuration) * 100;

                        $output->writeln("\t\tThis building is now <info>".round($progression, 2)."%</info> built");
                        // This building is over, we switch it from the ongoing to the realm
                        if($progression>=100) {
                            $realmBuilding = $this->getRepository('LLDCBundle:RealmBuilding')->findOneBy(array('realm' => $realm, 'type' => $building->getType()));
                            if(!is_null($realmBuilding)) {
                                $realmBuilding->setAmount($realmBuilding->getAmount()+1);
                            }
                            else {
                                $realmBuilding = new RealmBuilding();
                                $realmBuilding->setRealm($realm);
                                $realmBuilding->setType($building->getType());
                                $realmBuilding->setAmount(1);
                                $doctrine->persist($realmBuilding);
                            }
                            $output->writeln("\t\t1 ".$building->getBuilding()." done !");

                            // We notify the user that this building is built
                            $notification = new Notification();
                            $notification->setRealm($realm);
                            $notification->setContent(
                                $this->getContainer()->get('translator')->trans(
                                    'realm.academy.buildings.construction-over',
                                    array(
                                        'building.name' =>
                                        $this->getContainer()->get('translator')->trans('buildings.'.$realm->getCharacter()->getRace()->getLabel().'.'.$building->getType().'.one',
                                        array(),
                                        'buildings',
                                        $realm->getUser()->getLocale()),
                                    ),
                                    null,
                                    $realm->getUser()->getLocale()
                                )
                            );
                            $notification->setDateNotification($now);

                            $doctrine->persist($notification);
                            $doctrine->remove($building);
                            $this->getContainer()->get('lldc.ranking')->updatePoints($realm);
                            $peonAvailable += $peonOnThisOne;
                        }
                    }
                }
            }
            $this->getContainer()->get('lldc.ranking')->updatePointsPosition($game);
        }

        $doctrine->flush();

        $this->end($output);
    }
}
