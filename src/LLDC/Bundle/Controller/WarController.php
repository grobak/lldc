<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use LLDC\Bundle\Entity\War;

/**
 * Controller for the War page
 */
class WarController extends Controller
{
    /**
     * Displays the war page
     */
    public function indexAction() {
        return $this->render('LLDCBundle:War:war.html.twig', 
            array('attacking' => $this->getRealm()->getAttacking(), 'defending' => $this->getRealm()->getDefending())
        );
    }

    public function launchAttackAction(Request $request) {
        $warIdDefender = $request->get('warIdDefender', -1);
        if($warIdDefender==-1) {
            throw new \Exception('Select a realm to attack.');
        }
        $service = $this->container->get('lldc.war');
        $service->launchAttack($this->getRealm(), $this->getRepository('LLDCBundle:Realm')->findOneById($warIdDefender));
        $this->getManager()->flush();
        return $this->redirect($this->generateUrl('war'));
    }

    public function displayBattleAction($id) {
        /* @var $war War */
        $war = $this->getRepository("LLDCBundle:War")->findOneById($id);
        $realm = $this->getRealm();
        if (!$war->getAttackers()->contains($realm) && !$war->getDefenders()->contains($realm)) {
            throw new ForbiddenHttpException('Insufficient access');
        }
        return $this->render('LLDCBundle:War:battle.html.twig', ['war' => $war]);
    }

}
