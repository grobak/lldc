<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Controller\Webservice;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for the webservice providing the searchUser tool
 */
class SearchUserController extends WebserviceController
{
    private $options = array(
        'GAME_ONLY' => false,
        'SEARCH_BY_CHARACTER' => true,
        'SEARCH_BY_PLACE' => true,
        'SEARCH_BY_USER' => false
    );

    /**
     * Search a user with given parameters and options
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function searchUserAction(Request $request, $string, $options)
    {
        if($options != '') {
            $this->checkOptions($options);
        }

        $realmRepository = $this->container->get('doctrine')->getRepository('LLDCBundle:Realm');

        $matches = $realmRepository->searchUser(
            $string,
            $this->options['SEARCH_BY_USER'],
            $this->options['GAME_ONLY'],
            $this->options['SEARCH_BY_CHARACTER'],
            $this->options['SEARCH_BY_PLACE'],
            $this->options['GAME_ONLY'] ? $this->getGame() : null);

        $list = array();
        foreach($matches as $realm) {
            if(!isSet($list[$realm->getUser()->getId()])) {
                $list[$realm->getUser()->getId()] = array('user' => $realm->getUser(), 'realms' => array());
            }
            $list[$realm->getUser()->getId()]['realms'][] = array('game' => $realm->getGame(), 'realm' => $realm);
        }

        $response = new JsonResponse();
        $response->setData($list);

        return $response;
    }

    private function checkOptions($options) {
        $this->options['GAME_ONLY'] = $options[0]==1;
        $this->options['SEARCH_BY_CHARACTER'] = $options[1]==1;
        $this->options['SEARCH_BY_PLACE'] = $options[2]==1;
        $this->options['SEARCH_BY_USER'] = $options[3]==1;
    }
}
