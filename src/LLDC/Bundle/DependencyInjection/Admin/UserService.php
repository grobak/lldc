<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Admin
 */
namespace LLDC\Bundle\DependencyInjection\Admin;

use LLDC\Bundle\DependencyInjection\Service as Service;
use LLDC\Bundle\Entity\User;
use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\RealmBuilding;
use LLDC\Bundle\Entity\RealmProduction;
use LLDC\Bundle\Entity\RealmRanking;
use LLDC\Bundle\Entity\RealmResource;
use LLDC\Bundle\Entity\RpCharacter;
use LLDC\Bundle\Entity\RpPlace;
use LLDC\Bundle\Entity\Troop;

/**
 * Provides methods acting on the users.
 * For administration purpose
 */
class UserService extends Service {

    /**
     * Creates all the required rows for the user given.
     * Be careful, the $user has to exist in database first !
     * @param User The user
     * @return void
     */
    public function createUser(User $user) {
        $doctrine = $this->getManager();

        // Fetching the main game to initialize realm settings
        $game = $this->getRepository('LLDCBundle:Game')->findOneByMain(1);
        $race = $this->getRepository('LLDCBundle:Race')->findOneByLabel($user->getRegistrationRace());
        $gender = $this->getRepository('LLDCBundle:Gender')->findOneByLabel($user->getRegistrationGender());

        $rpCharacter = new RpCharacter();
        $rpCharacter->setUser($user);
        $rpCharacter->setName($user->getUsername());
        $rpCharacter->setRace($race);
        $rpCharacter->setGender($gender);
        $rpCharacter->setDisplayed(true);
        $rpCharacter->setDefaultForum(true);
        $rpCharacter->setDeleted(false);

        $rpPlace = new RpPlace();
        $rpPlace->setUser($user);
        $rpPlace->setName($user->getRegistrationRealmName());

        // Creating the realm
        $realm = new Realm();
        $realm->setUser($user);
        $realm->setGame($game);
        $realm->setDateCreation(new \DateTime());
        $realm->setCharacter($rpCharacter);
        $realm->setPlace($rpPlace);

        /**
         * Creation of all the buildings for this realm, depending on its race and the game
         */
        foreach($this->getRepository('LLDCBundle:GameInitialBuilding')->findBy(array('game' => $game, 'race' => $race)) as $initialBuilding) {
            $realmBuilding = new RealmBuilding();
            $realmBuilding->setRealm($realm);
            $realmBuilding->setType($initialBuilding->getType());
            $realmBuilding->setAmount($initialBuilding->getAmount());
            $doctrine->persist($realmBuilding);
        }

        /**
         * Creation of all the troops for this realm, depending on its race and the game
         */
        foreach($this->getRepository('LLDCBundle:GameInitialTroop')->findBy(array('game' => $game, 'race' => $race)) as $intialTroop) {
            $troop = new Troop();
            $troop->setType($intialTroop->getType());
            $troop->setAmount($intialTroop->getAmount());
            $troop->setRealm($realm);
            $troop->setDateCreation(new \DateTime());
            $troop->setRace($race);
            $doctrine->persist($troop);
        }

        $realmResource = new RealmResource();
        $realmResource->setRealm($realm);
        $realmResource->setFood($game->getInitialResourcesFood());
        $realmResource->setWood($game->getInitialResourcesWood());
        $realmResource->setRock($game->getInitialResourcesRock());
        $realmResource->setGold($game->getInitialResourcesGold());
        $realmResource->setMadrens($game->getInitialResourcesMadrens());
        $realmResource->setMorale($game->getInitialResourcesMorale());
        $realmResource->setXp($game->getInitialResourcesXp());

        $realmProduction = new RealmProduction();
        $realmProduction->setRealm($realm);
        $realmProduction->setFoodProdPercent($game->getInitialProductionFood());
        $realmProduction->setWoodProdPercent($game->getInitialProductionWood());
        $realmProduction->setRockProdPercent($game->getInitialProductionRock());
        $realmProduction->setGoldProdPercent($game->getInitialProductionGold());
        $realmProduction->setBuildingProdPercent($game->getInitialProductionBuilding());

        foreach($this->getContainer()->getParameter('lldc')['global']['ranking']['type'] as $type) {
            $realmRanking = new RealmRanking();
            $realmRanking->setRealm($realm);
            $realmRanking->setType($type);
            $realmRanking->setPosition(0);
            $realmRanking->setValue(0);
            $doctrine->persist($realmRanking);
        }

        // Inserts
        $doctrine->persist($rpCharacter);
        $doctrine->persist($rpPlace);
        $doctrine->persist($realm);
        $doctrine->persist($realmResource);
        $doctrine->persist($realmProduction);
        // And commit
        $doctrine->flush();

        /*
         * We add a default battlefield to this new realm
         * It has to be AFTER the flush, because addBattlefield needs an existant place for its SQL request
         */
        $this->getContainer()->get('lldc.battlefield')->addBattlefield($realm);
        $doctrine->flush();
    }

    /**
     * Delete the given user
     * @param User The user to delete
     * @return void
     */
    public function deleteUser(User $user)
    {
        $em = $this->getManager();

        // We delete the realms
        $realms = $user->getRealms();
        foreach($realms as $realm) {
            $em->remove($realm->getProduction());
            $em->remove($realm->getResource());

            foreach($realm->getResearches() as $research) {
                $em->remove($research);
            }

            foreach($realm->getTroop() as $troop) {
                $em->remove($troop);
            }

            foreach($realm->getBuilding() as $building) {
                $em->remove($building);
            }

            foreach($realm->getRanking() as $ranking) {
                $em->remove($ranking);
            }

            $em->remove($realm);
        }

        foreach($user->getCharacters() as $character) {
            $em->remove($character);
        }
        foreach($user->getPlaces() as $place) {
            foreach($place->getBattlefields() as $bf) {
                foreach($bf->getSquares() as $square) {
                    $em->remove($square);
                }
                $em->remove($bf);
            }
        }
        foreach($user->getPlaces() as $place) {
            $em->remove($place);
        }

        $em->remove($user);

        $em->flush();
    }
}
