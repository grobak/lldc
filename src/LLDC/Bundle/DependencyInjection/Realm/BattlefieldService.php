<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */

namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;
use LLDC\Bundle\Entity\Battlefield as Battlefield;
use LLDC\Bundle\Entity\BattlefieldSquare as BattlefieldSquare;
use LLDC\Bundle\Entity\Realm as Realm;
use LLDC\Bundle\Entity\RpPlace as RpPlace;

/**
 * Provides methods acting on the Battlefield and BattlefieldSquare entities.
 * Allows you to generate new battlefields...
 */
class BattlefieldService extends Service {

    /**
     * This method adds a new battlefield to the specified realm.
     *
     * @param Realm $realm
     *
     * @return Battlefield
     */
    public function addBattlefield(Realm $realm) {
        $place = $realm->getPlace();
        $game = $realm->getGame();

        $manager = $this->getManager();
        $bfCount = $manager
                ->createQuery(
                        'SELECT COUNT(bf.id) as bfCount FROM LLDCBundle:Battlefield bf WHERE bf.place=:place'
                )
                ->setParameter('place', $place)
                ->getSingleScalarResult();

        if ($bfCount >= $game->getBattlefieldsMaxAmount())
            return null;

        $battlefield = new Battlefield();
        $battlefield->setPlace($place);
        $manager->persist($battlefield);

        // We divide it by 2 because 0,0 ought to be at the center, not the upper corner.
        $size = $game->getBattlefieldsSize() / 2;
        $randomPool = array(); // To easily pick a random square
        $cube = array(); // To easily access squares by cubic coordinates
        // First, let's layout grass all over the place
        for ($i = -$size; $i < $size; $i++) {
            $upper_limit = min($size - $i, $size);
            for ($j = max(-$size - $i, -$size); $j < $upper_limit; $j++) {
                $square = new BattlefieldSquare();
                $square->setQ($i);
                $square->setR($j);
                // XXX: Hardcoding is BAD!
                $square->setGroundType('grass');
                $square->setBattlefield($battlefield);
                $manager->persist($square);

                $randomPool[] = $square;

                $x = $square->getX();
                $y = $square->getY();
                $z = $square->getZ();
                if (!isSet($cube[$x]))
                    $cube[$x] = array();
                if (!isSet($cube[$x][$y]))
                    $cube[$x][$y] = array();
                $cube[$x][$y][$z] = $square;
            }
        }

        // XXX: This is a truly terrible way to randomize it all!!
        for ($count = mt_rand(0, 255); $count > 0; $count--) {
            $key = array_rand($randomPool);
            // This really shouldn't be hardcoded either.
            $randomPool[$key]->setGroundType('hill');
        }
        return $battlefield;
    }

    public function removeBattlefield(Battlefield $battlefield) {
        // Retrieve the LLDC configuration
        $lldc = $this->getLLDC();

        $manager = $this->getManager();
        $squares = $this->getRepository('LLDCBundle:BattlefieldSquare')->findBy(array("battlefield" => $battlefield));
        foreach ($squares as $s)
            $manager->remove($s);
        $manager->remove($battlefield);
    }

}
