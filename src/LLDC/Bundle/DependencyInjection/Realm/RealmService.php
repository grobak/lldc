<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;

use LLDC\Bundle\LLDCException;

use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\Notification;

/**
 * Provides methods acting on the realms.
 */
class RealmService extends Service {

    /**
     * Notify a realm
     * @param $type Type of the notification (@see lldc.yml ==> lldc.notification.types)
     */
    public function notify(Realm $realm, $message, $type = null, $path = null) {
        if(is_null($type)) {
            $type = $this->getLLDC()['notification']['default-type'];
        }
        elseif(!in_array($type, $this->getLLDC()['notification']['types'])) {
            throw new LLDCException('Notification type "'.$type.'" not found. Be sure to use one of these : '.print_r($this->getLLDC()['notification']['types'], true));
        }

        if(is_null($message)) {
            throw new LLDCException('You need to specify a message for this notification');
        }

        $token = $this->getContainer()->get('security.context')->getToken();
        /**
         * If there is a security token, the method is executed from a user context
         * We store in DB if the current user isn't the one we want notify, or if the $path is given (we can't handle the path in flashbag)
         */
        if(!is_null($token) && $realm->getUser()===$token->getUser() && is_null($path)) {
            $this->getContainer()->get('session')->getFlashBag()->add($type, $message);
        }
        else {
            $notification = new Notification();
            $notification->setRealm($realm);
            $notification->setContent($message);
            $notification->setDateNotification(new \DateTime());
            $notification->setType($type);
            $notification->setPath($path);
            $this->getManager()->persist($notification);
        }
    }

    public function isOnline(Realm $realm) {
        $now = new \Datetime();
        return $realm->getUser()->getDateLastAction() > $now->sub(new \Dateinterval("PT300S"));
    }

}
