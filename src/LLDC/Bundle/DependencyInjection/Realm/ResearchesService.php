<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;

use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\OngoingResearch;

use LLDC\Bundle\Util\ResearchBean;
use LLDC\Bundle\Exception\LLDCException;
use LLDC\Bundle\Util\ResourcesBean;

/**
 * Provides methods acting on the realm's researches.
 */
class ResearchesService extends Service {

    /*
     * This static array contains all researches
     * @var array(ResearchBean)
     */
    private static $researches;

    public function __construct($container) {
        parent::__construct($container);
        $this->hydrate();
    }

    /**
     * Hydrates the static list of all researches
     */
    private function hydrate() {
        $lldc = $this->getLLDC();
        $list = array();

        // $part can be buildings, market, production, researches, spy, units, war...
        foreach($lldc['researches'] as $part => $researches) {
            if($researches != null) {
                // More detailed type of the part, with its associated researches (for example : production.food / wood / global ...)
                foreach($researches as $type => $research) {
                    // Now, we have the research with its configuration
                    foreach($research as $name => $data) {
                        $data['part'] = $part;
                        $data['type'] = $type;
                        $data['name'] = $name;
                        $list[$part.'.'.$type.'.'.$name] = self::getResearchBean($data);
                    }
                }
            }
        }

        // "Topological" sort starts here
        $researchesWithoutNeed = array();
        $finalList = array();

        // We retrive all researches without need
        foreach ($list as $key => $researchBean) {
            if($researchBean->hasRequirements()) {
                $researchesWithoutNeed[$key] = $researchBean;
            }
        }
        
        // While requirements aren't satisfied
        while($researchesWithoutNeed) {
            foreach($researchesWithoutNeed as $key => $researchBean) {
                $finalList[$key] = $researchBean;
            }
            $researchesWithoutNeed = array();

            foreach($list as $key=>$researchBean) {
                if(!isSet($finalList[$key])) {
                    $satisfiedDependency = true;
                    if($researchBean->hasRequirements()) {
                        foreach($researchBean->getRequire() as $require) {
                            if(!isSet($finalList[$require])) {
                                $satisfiedDependency = false;
                            }
                        }
                    }
                    if($satisfiedDependency) {
                        $researchesWithoutNeed[$key] = $researchBean;
                    }
                }
            }
        }

        /**
         * We handle the dependencies
         */
        foreach($finalList as $research) {
            if($research->hasRequirements()) {
                foreach($research->getRequire() as $require) {
                    $list[$require]->addSuccessor($research);
                }
            }
        }

        self::$researches = $list;
    }

    /**
     * Returns an array containing all available researches for the realm given
     * @param Realm $realm
     * @return array(ResearchBean)
     */
    public function getAvailableResearches(Realm $realm) {
        $available = array();
        $performed = self::getPerformedResearches($realm);
        $performedResearchesKeys = self::getPerformedResearchesKeys($realm);

        // We add all researches without requirements which aren't performed yet
        foreach(self::$researches as $researchBean) {
            if(!$researchBean->hasRequirements() && !in_array($researchBean->getFullname(), $performedResearchesKeys)) {
                $available[$researchBean->getFullname()] = $researchBean;
            }
        }

        // We unlock researches which require this one
        foreach($performed as $researchBean) {
            if($researchBean->hasSuccessor()) {
                foreach($researchBean->getSuccessor() as $successor) {
                    $researchSuccessor = self::$researches[$successor->getFullname()];
                    // If it's already perform or in the array of available researches, we just don't care
                    if(!in_array($researchSuccessor->getFullname(), $performedResearchesKeys) && !array_key_exists($researchSuccessor->getFullname(), $available)) {
                        // If this one only has one requirement
                        if(count($researchSuccessor->getRequire()) == 1) {
                            $available[$researchSuccessor->getFullname()] = $researchSuccessor;
                        }
                        // Else, we have to check if all requirements are satisfied
                        else {
                            $unlockIt = true;
                            foreach($researchSuccessor->getRequire() as $requirement) {
                                if($unlockIt && !in_array($requirement, $performedResearchesKeys)) {
                                    $unlockIt = false;
                                }
                            }
                            if($unlockIt) {
                                $available[$researchSuccessor->getFullname()] = $researchSuccessor;
                            }
                        }
                    }
                }
            }
        }

        // If there is an ongoing research, we have to delete it from the available researches array
        if($this->hasOngoingResearch($realm)) {
            if(array_key_exists($this->getOngoingResearch($realm)->getFullname(), $available)) {
                unset($available[$this->getOngoingResearch($realm)->getFullname()]);
            }
        }

        return $available;
    }

    /**
     * Returns an array containing all performed researches for the realm given
     * @param Realm $realm
     * @return array(ResearchBean)
     */
    public function getPerformedResearches(Realm $realm) {
        $performedResearches = array();

        foreach($realm->getResearches() as $done) {
            array_push($performedResearches, self::$researches[$done->getResearch()]);
        }

        return $performedResearches;
    }

    /**
     * Returns an array containing all keys of performed researches for the realm given
     * @param Realm $realm
     * @return array(String)
     */
    public function getPerformedResearchesKeys(Realm $realm) {
        $performedResearchesKeys = array();

        foreach($realm->getResearches() as $done) {
            array_push($performedResearchesKeys, $done->getResearch());
        }

        return $performedResearchesKeys;
    }

    /**
     * Returns a bean representing the research
     * 
     * @param array(mixed)
     * @return ResearchBean 
     */
    private function getResearchBean($research) {
        try {
            return new ResearchBean(
                $research['part'], $research['type'], $research['name'],
                $research['cost']['food'], $research['cost']['wood'], 
                $research['cost']['rock'], $research['cost']['gold'],
                $research['cost']['madrens'], $research['cost']['xp'],
                $research['cost']['points'], $research['duration'],
                $research['value'], isSet($research['require']) ? $research['require'] : null);
        }
        catch(\Exception $e) {
            new LLDCException($this->getContainer()->get('translator')->trans('technical.researches.syntax'), $e);
        }
    }

    /**
     * Throws an exception if the research isn't available for the realm
     * @param String researchId
     * @param Realm realm
     */
    private function isAvailable($researchId, Realm $realm) {
        if(!array_key_exists($researchId, $this->getAvailableResearches($realm))) {
            throw new \Exception($this->getContainer()->get('translator')->trans('realm.researches.todo.launched.unavailable'));
        }
    }

    /**
     * Returns true if the realm has an ongoing research
     * @return boolean
     */
    public function hasOngoingResearch(Realm $realm) {
        return $realm->getOngoingResearch() != null;
    }

    /**
     * Throws an exception if the research isn't available for the realm
     * @param Realm realm
     */
    private function canLaunchResearch(Realm $realm) {
        if($this->hasOngoingResearch($realm)) {
            throw new \Exception($this->getContainer()->get('translator')->trans('realm.researches.todo.launched.alreadyOne'));
        }
    }

    /**
     * Returns the ongoing research if there is one
     * @param Realm realm
     * @return ResearchBean ongoingResearch
     */
    public function getOngoingResearch(Realm $realm) {
        if($this->hasOngoingResearch($realm)) {
            $research = self::$researches[$realm->getOngoingResearch()->getResearch()];
            $research->setOngoing($realm->getOngoingResearch());
            return $research;
        }
        else {
            return null;
        }
    }

    /**
     * Starts the research given for the realm
     * @param String researchId
     * @param Realm realm
     */
    public function launchResearch($researchId, Realm $realm) {
        // If there is already an ongoing research, we throw an exception
        $this->canLaunchResearch($realm);

        $research = self::$researches[$researchId];

        // We verify if the research is available for this realm
        $this->isAvailable($researchId, $realm);

        $costs = new ResourcesBean();
        $costs->setFood($research->getFood());
        $costs->setWood($research->getWood());
        $costs->setGold($research->getGold());
        $costs->setRock($research->getRock());
        $costs->setMadrens($research->getMadrens());

        // We take the resources needed
        $this->getContainer()->get('lldc.resources')->take($costs);

        // We create the OngoingResearch object
        $ongoingResearch = new OngoingResearch();
        $ongoingResearch->setRealm($realm);
        $ongoingResearch->setResearch($research->getFullname());
        $ongoingResearch->setDateBegin(new \DateTime());
        $dateEnd = new \DateTime();
        $dateEnd->add(\DateInterval::createFromDateString($research->getDuration().' minutes'));
        $ongoingResearch->setDateEnd($dateEnd);

        // Persist the modifications
        $this->getContainer()->get('doctrine')->getManager()->persist($ongoingResearch);
        $this->getContainer()->get('doctrine')->getManager()->flush();
    }
}
