<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\DependencyInjection\Realm
 */
namespace LLDC\Bundle\DependencyInjection\Realm;

use LLDC\Bundle\DependencyInjection\Service as Service;

use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\Troop;

use LLDC\Bundle\LLDCEvents;
use LLDC\Bundle\LLDCException;
use LLDC\Bundle\Event\TroopEvent;

use LLDC\Bundle\Util\ResourcesBean;

use Doctrine\ORM\UnitOfWork;

/**
 * Provides methods acting on the troop entity.
 * Allows you to add, demobilize, and so much more !
 */
class TroopService extends Service {

    /**
     * This method buys the given units for the current realm/game.
     *
     * @param troop $troop - Units to buy
     * @param Realm $realm - If given, buy the units for this realm, otherwise, the realm is retrived with the user/game in session - <b>Default : null</b>
     *
     * @throws Exception - Not enough resources
     *
     * @return true
     */
    public function buyUnits(Troop $troop, Realm $realm = null)
    {
        // Fetch the current realm if needed
        if(is_null($realm)) {
            $realm = $this->getRepository('LLDCBundle:Realm')->findOneBy(array('game' => $this->getGame(), 'user' => $this->getUser()));
        }

        // Calculate the costs of the given units
        $costs = $this->getCosts($troop, $realm);

        // Take resources from the RealmResources entity, using the resources service
        $this->getContainer()->get('lldc.resources')->take($costs);

        // Add the units to the Realm->troop
        $troopAtHome = $this->getRepository('LLDCBundle:troop')->findOneAvailableByRealmAndAndType($realm, $troop->getType());
        if(isSet($troopAtHome[0])) {
        	$troopAtHome[0]->setAmount($troopAtHome[0]->getAmount() + $troop->getAmount());
        }
        else {
        	$newTroop = new troop();
        	$newTroop->setType($troop->getType());
        	$newTroop->setAmount($troop->getAmount());
        	$newTroop->setDateCreation(new \DateTime());
        	$newTroop->setRealm($realm);
               $newTroop->setRace($realm->getCharacter()->getRace());
        	$this->getManager()->persist($newTroop);
        }

        $this->getEventDispatcher()->dispatch(LLDCEvents::ACADEMY_UNIT_BOUGHT, new TroopEvent($troop));

        // Persist the modifications
        $this->getManager()->flush();

        return true;
    }

    /**
     * This method adds a troop.
     *
     * @param Realm $realm - The realm
     * @param string $type - The type of the troop to create
     * @param integer $amount - The amount of units in the troop
     *
     * @return ResourcesBean
     */
    public function add(Realm $realm, $type, $amount) {
        if($amount<0) {
            throw new \Exception('The amount of units in the troop can\'t be negative.');
        }
        // TODO - check the type / race
        $newTroop = new Troop();
        $newTroop->setType($type);
        $newTroop->setAmount($amount);
        $newTroop->setDateCreation(new \DateTime());
        $newTroop->setRace($realm->getCharacter()->getRace());
        $newTroop->setRealm($realm);
        $this->getManager()->persist($newTroop);

        return $newTroop;
    }

    /**
     * This method calculates the cost of the given units.
     *
     * @param Troop $troop - The troop
     * @param Realm $realm - The realm for which we wanna calculate the cost
     *
     * @return ResourcesBean
     */
    public function getCosts(Troop $troop, Realm $realm)
    {
        // Retrieve the LLDC's configuration
        $lldc = $this->getLLDC();

        // Cost's ratio for the current game
        $ratio = $realm->getGame()->getUnitsCost();

        // Fetching costs
        $cost = $lldc['units'][$realm->getCharacter()->getRace()->getLabel()][$troop->getType()]['cost']['resources'];

        $resources = new ResourcesBean();

        // Calculating resources needed
        $resources->setFood($troop->getAmount()*$cost['food']*$ratio);
        $resources->setWood($troop->getAmount()*$cost['wood']*$ratio);
        $resources->setGold($troop->getAmount()*$cost['gold']*$ratio);
        $resources->setRock($troop->getAmount()*$cost['rock']*$ratio);
        $resources->setMadrens($troop->getAmount()*$cost['madrens']*$ratio);

        // Return the resources needed
        return $resources;
    }

    /**
     * This method calculates the cost of the maintenance for a realm.
     *
     * @param Realm $realm - The realm for which we wanna calculate the cost
     * @param int $gameFactor
     * @param float $gameDuringInternalWar
     * @param float $gameDuringExternalWar
     * @param array(mixed) $raceConf
     * @param integer $hours (default : 1) - Cost for n hours
     *
     * @return ResourcesBean
     */
    public function getMaintenanceCost(Realm $realm, $gameFactor = null, $gameDuringInternalWar = null, $gameDuringExternalWar = null, $raceConf = null, $hours = 1)
    {
        if(is_null($raceConf)) {
            $lldc = $this->getLLDC();
            $raceConf = $lldc['units'][$realm->getCharacter()->getRace()->getLabel()];
        }
        if(is_null($gameFactor)) {
            $gameFactor = $realm->getGame()->getUnitsMaintenanceFactor();
        }
        if(is_null($gameDuringInternalWar)) {
            $gameDuringInternalWar = $game->getUnitsMaintenanceDuringInternalWar();
        }
        if(is_null($gameDuringExternalWar)) {
            $gameDuringExternalWar = $game->getUnitsMaintenanceDuringExternalWar();
        }

        // We calculate the maintenance cost for the troops
        $weightedUnits = $this->getRepository('LLDCBundle:troop')->preCalculateMaintenanceCost($realm, $gameFactor, $gameDuringInternalWar, $gameDuringExternalWar);
        $costs = array('food' => 0, 'wood' => 0, 'rock' => 0, 'gold' => 0, 'madrens' => 0, 'xp' => 0);
        foreach($weightedUnits as $units) {
            $type = $units['type'];
            $costs['food'] += $units['weightUnits']*$raceConf[$type]['cost']['maintenance']['food'];
            $costs['wood'] += $units['weightUnits']*$raceConf[$type]['cost']['maintenance']['wood'];
            $costs['rock'] += $units['weightUnits']*$raceConf[$type]['cost']['maintenance']['rock'];
            $costs['gold'] += $units['weightUnits']*$raceConf[$type]['cost']['maintenance']['gold'];
            $costs['madrens'] += $units['weightUnits']*$raceConf[$type]['cost']['maintenance']['madrens'];
            $costs['xp'] += $units['weightUnits']*$raceConf[$type]['cost']['maintenance']['xp'];
        }

        $resources = new ResourcesBean();
        // Calculating resources needed
        $resources->setFood($costs['food']/24/$hours);
        $resources->setWood($costs['wood']/24/$hours);
        $resources->setGold($costs['gold']/24/$hours);
        $resources->setRock($costs['rock']/24/$hours);
        $resources->setMadrens($costs['madrens']/24/$hours);
        $resources->setXp($costs['xp']/24/$hours);

        // Return the resources needed
        return $resources;
    }

	/**
	 *	Splits a troop in two separates troops.
	 *
	 * @param \LLDC\Bundle\Entity\troop $troop The troop to split
	 * @param double $ratio The percentage of this troop that we wanna split
	 * @param double $parity
	 * @return \LLDC\Bundle\Entity\troop The new troop
	 *
	 * @throw Exception The wanted type doesn't exist for this race
	 *
	 * Example
	 *  troop1 : 1 000 peon
	 *  split(troop1, 0.2, 3, a1) returns troop2
	 *  ---> troop1 : 800 peon / troop2 : 600 a1
	 */
	public function split(Troop $troop, $ratio = 0.5, $parity = 1, $type = null) {

        $ratio = is_null($ratio) || empty($ratio) ? 0.5 : $ratio;
        $parity = is_null($parity) || empty($parity) ? 1 : $parity;
        $type = empty($type) ? null : $type;

		$newTroop = new Troop();
		$newTroop->setDateCreation(new \DateTime());
               $newTroop->setRace($troop->getRace());
		$newTroop->setRealm($troop->getRealm());
		$newTroop->setAmount($troop->getAmount()*$ratio*$parity);
		if(is_null($type)) {
			$newTroop->setType($troop->getType());
		}
		else {
			$conf = $this->getLLDC();
			if(isSet($conf['units'][$troop->getRealm()->getCharacter()->getRace()->getLabel()][$type])) {
				$newTroop->setType($type);
			}
			else {
				throw new \Exception('The selected type doesn\'t exist for this race.');
			}
		}
		$newTroop->setWar($troop->getWar());

		$troop->setAmount($troop->getAmount()*(1-$ratio));

		$this->getManager()->persist($newTroop);

        $eventType = !is_null($troop->getWar()) ? LLDCEvents::WAR_TROOP_SPLIT : LLDCEvents::TROOP_SPLIT;
        $this->getEventDispatcher()->dispatch($eventType, new TroopEvent($troop, $newTroop));

		return $newTroop;
	}

    /**
     *	Merge two troops together.
     *
     * @param \LLDC\Bundle\Entity\troop $troopToMerge The troop to merge
     * @param \LLDC\Bundle\Entity\troop $troop The troop to merge with.
     * @return \LLDC\Bundle\Entity\troop The new troop
     *
     * @throw Exception
     */
    public function merge(Troop $troopToMerge, Troop $troop) {
        if($this->getManager()->getUnitOfWork()->isScheduledForDelete($troopToMerge)) {
            throw new \Exception('The troop to merge has already been removed.');
        }
        elseif($this->getManager()->getUnitOfWork()->isScheduledForUpdate($troopToMerge)) {
            throw new \Exception('The troop to merge is being updated.');
        }
        elseif($troop->getRealm() != $troopToMerge->getRealm()) {
            throw new \Exception('Unmergeable troops... They don\'t belong to the same realm.');
        }
        elseif($troop->getType() != $troopToMerge->getType()) {
            throw new \Exception('Unmergeable troops... Incompatible types.');
        }

        $troop->setAmount($troop->getAmount()+$troopToMerge->getAmount());
        $troopToMerge->setAmount(0);
        $this->getManager()->remove($troopToMerge);

        return $troop;
    }

    public function getAbilities(Troop $troop) {
		$lldc = $this->getLLDC();

		return $lldc['units'][$troop->getRealm()->getCharacter()->getRace()->getLabel()][$troop->getType()]['abilities'];
    }

    /**
     * @throw LLDCException
     */
	public function abilityAvailable($abilityName, Troop $t = null) {
		$lldc = $this->getLLDC();

		$ability = isSet($lldc['abilities'][$abilityName]) ? $lldc['abilities'][$abilityName] : null;
		if(is_null($ability)) {
			throw new LLDCException('The ability doesn\'t exist.', LLDCException::ABILITY_NOT_EXISTS);
		}

		if(!is_null($t)) {
			$tParam = $lldc['units'][$t->getRealm()->getCharacter()->getRace()->getLabel()][$t->getType()];

			if(!in_array($abilityName, $tParam['abilities'])) {
				throw new LLDCException('The ability isn\'t available for this troop.', LLDCException::ABILITY_NOT_AVAILABLE_FOR_TROOP);
			}
		}

		return $ability;
	}

    /**
     * @throw LLDCException
     */
	public function attack(Troop $a, Troop $b, $abilityName) {
		// We get the ability (and check if it's available for the attacking troop)
		$ability = $this->abilityAvailable($abilityName, $a);

		// We verify if the ability is an attack - Well, they all are attacks now, right ?
		// if(!isSet($ability['attack']) || !$ability['attack']) {
		// 	throw new LLDCException('The ability "'.$abilityName.'" isn\'t an attack.', LLDCException::ABILITY_NOT_AN_ATTACK);
		// }
		if($a->getRealm() === $b->getRealm()) { // So it can be the same troop too ---> Exception
			throw new LLDCException('The troops belong to the same realm, there is no friendly fire.', LLDCException::TROOP_FRIENDLY_FIRE);
		}

		$lldc = $this->getLLDC();
		$aParam = $lldc['units'][$a->getRealm()->getCharacter()->getRace()->getLabel()][$a->getType()];
		$bParam = $lldc['units'][$b->getRealm()->getCharacter()->getRace()->getLabel()][$b->getType()];

        $this->getEventDispatcher()->dispatch(LLDCEvents::TROOP_TRIGGER_BEFORE_ATTACK, new TroopEvent($a, $b));
        $this->getEventDispatcher()->dispatch(LLDCEvents::TROOP_TRIGGER_BEFORE_DEFEND, new TroopEvent($b, $a));

        $a->setAmount($a->getAmount()-10);
        $b->setAmount($b->getAmount()-25);

        $this->getEventDispatcher()->dispatch(LLDCEvents::TROOP_TRIGGER_AFTER_ATTACK, new TroopEvent($a, $b));
        $this->getEventDispatcher()->dispatch(LLDCEvents::TROOP_TRIGGER_AFTER_DEFEND, new TroopEvent($b, $a));
	}
}
