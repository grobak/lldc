<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Battlefield
 */
class Battlefield
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \LLDC\Bundle\Entity\RpPlace
     */
    private $place;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param \LLDC\Bundle\Entity\RpPlace $place
     * @return Battlefield
     */
    public function setPlace(\LLDC\Bundle\Entity\RpPlace $place = null)
    {
        $this->place = $place;
    
        return $this;
    }

    /**
     * Get place
     *
     * @return \LLDC\Bundle\Entity\RpPlace 
     */
    public function getPlace()
    {
        return $this->place;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $squares;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->squares = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add squares
     *
     * @param \LLDC\Bundle\Entity\BattlefieldSquare $squares
     * @return Battlefield
     */
    public function addSquare(\LLDC\Bundle\Entity\BattlefieldSquare $squares)
    {
        $this->squares[] = $squares;

        return $this;
    }

    /**
     * Remove squares
     *
     * @param \LLDC\Bundle\Entity\BattlefieldSquare $squares
     */
    public function removeSquare(\LLDC\Bundle\Entity\BattlefieldSquare $squares)
    {
        $this->squares->removeElement($squares);
    }

    /**
     * Get squares
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSquares()
    {
        return $this->squares;
    }
}
