<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 */
class Game implements \JsonSerializable
{   
    /**
     * JSon serialization
     */
    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'main' => $this->main,
            'label' => $this->label
        );
    }
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $main;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $dateStart;

    /**
     * @var \DateTime
     */
    private $dateEnd;

    /**
     * @var integer
     */
    private $initialResourcesFood;

    /**
     * @var integer
     */
    private $initialResourcesWood;

    /**
     * @var integer
     */
    private $initialResourcesRock;

    /**
     * @var integer
     */
    private $initialResourcseGold;

    /**
     * @var integer
     */
    private $initialResourcesMadrens;

    /**
     * @var integer
     */
    private $initialResourcesArpent;

    /**
     * @var integer
     */
    private $initialProductionFood;

    /**
     * @var integer
     */
    private $initialProductionWood;

    /**
     * @var integer
     */
    private $initialProductionRock;

    /**
     * @var integer
     */
    private $initialProductionGold;

    /**
     * @var integer
     */
    private $initialProductionBuilding;

    /**
     * @var integer
     */
    private $buildingsCost;

    /**
     * @var integer
     */
    private $buildingsQueueLength;

    /**
     * @var integer
     */
    private $buildingsSpeed;

    /**
     * @var float
     */
    private $productionRateFood;

    /**
     * @var float
     */
    private $productionRateWood;

    /**
     * @var float
     */
    private $productionRateRock;

    /**
     * @var float
     */
    private $productionRateGold;

    /**
     * @var float
     */
    private $productionRateMadrens;

    /**
     * @var integer
     */
    private $researchesCost;

    /**
     * @var integer
     */
    private $researchesSpeed;

    /**
     * @var integer
     */
    private $unitsCapacity;

    /**
     * @var integer
     */
    private $unitsCost;

    /**
     * @var integer
     */
    private $unitsMaintenance;

    /**
     * @var integer
     */
    private $unitsPoints;

    /**
     * @var integer
     */
    private $unitsSpeed;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set main
     *
     * @param boolean $main
     * @return Game
     */
    public function setMain($main)
    {
        $this->main = $main;
    
        return $this;
    }

    /**
     * Get main
     *
     * @return boolean 
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Game
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Game
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Game
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set initialResourcesFood
     *
     * @param integer $initialResourcesFood
     * @return Game
     */
    public function setInitialResourcesFood($initialResourcesFood)
    {
        $this->initialResourcesFood = $initialResourcesFood;
    
        return $this;
    }

    /**
     * Get initialResourcesFood
     *
     * @return integer 
     */
    public function getInitialResourcesFood()
    {
        return $this->initialResourcesFood;
    }

    /**
     * Set initialResourcesWood
     *
     * @param integer $initialResourcesWood
     * @return Game
     */
    public function setInitialResourcesWood($initialResourcesWood)
    {
        $this->initialResourcesWood = $initialResourcesWood;
    
        return $this;
    }

    /**
     * Get initialResourcesWood
     *
     * @return integer 
     */
    public function getInitialResourcesWood()
    {
        return $this->initialResourcesWood;
    }

    /**
     * Set initialResourcesRock
     *
     * @param integer $initialResourcesRock
     * @return Game
     */
    public function setInitialResourcesRock($initialResourcesRock)
    {
        $this->initialResourcesRock = $initialResourcesRock;
    
        return $this;
    }

    /**
     * Get initialResourcesRock
     *
     * @return integer 
     */
    public function getInitialResourcesRock()
    {
        return $this->initialResourcesRock;
    }

    /**
     * Set initialResourcseGold
     *
     * @param integer $initialResourcseGold
     * @return Game
     */
    public function setInitialResourcseGold($initialResourcseGold)
    {
        $this->initialResourcseGold = $initialResourcseGold;
    
        return $this;
    }

    /**
     * Get initialResourcseGold
     *
     * @return integer 
     */
    public function getInitialResourcseGold()
    {
        return $this->initialResourcseGold;
    }

    /**
     * Set initialResourcesMadrens
     *
     * @param integer $initialResourcesMadrens
     * @return Game
     */
    public function setInitialResourcesMadrens($initialResourcesMadrens)
    {
        $this->initialResourcesMadrens = $initialResourcesMadrens;
    
        return $this;
    }

    /**
     * Get initialResourcesMadrens
     *
     * @return integer 
     */
    public function getInitialResourcesMadrens()
    {
        return $this->initialResourcesMadrens;
    }

    /**
     * Set initialResourcesArpent
     *
     * @param integer $initialResourcesArpent
     * @return Game
     */
    public function setInitialResourcesArpent($initialResourcesArpent)
    {
        $this->initialResourcesArpent = $initialResourcesArpent;
    
        return $this;
    }

    /**
     * Get initialResourcesArpent
     *
     * @return integer 
     */
    public function getInitialResourcesArpent()
    {
        return $this->initialResourcesArpent;
    }

    /**
     * Set initialProductionFood
     *
     * @param integer $initialProductionFood
     * @return Game
     */
    public function setInitialProductionFood($initialProductionFood)
    {
        $this->initialProductionFood = $initialProductionFood;
    
        return $this;
    }

    /**
     * Get initialProductionFood
     *
     * @return integer 
     */
    public function getInitialProductionFood()
    {
        return $this->initialProductionFood;
    }

    /**
     * Set initialProductionWood
     *
     * @param integer $initialProductionWood
     * @return Game
     */
    public function setInitialProductionWood($initialProductionWood)
    {
        $this->initialProductionWood = $initialProductionWood;
    
        return $this;
    }

    /**
     * Get initialProductionWood
     *
     * @return integer 
     */
    public function getInitialProductionWood()
    {
        return $this->initialProductionWood;
    }

    /**
     * Set initialProductionRock
     *
     * @param integer $initialProductionRock
     * @return Game
     */
    public function setInitialProductionRock($initialProductionRock)
    {
        $this->initialProductionRock = $initialProductionRock;
    
        return $this;
    }

    /**
     * Get initialProductionRock
     *
     * @return integer 
     */
    public function getInitialProductionRock()
    {
        return $this->initialProductionRock;
    }

    /**
     * Set initialProductionGold
     *
     * @param integer $initialProductionGold
     * @return Game
     */
    public function setInitialProductionGold($initialProductionGold)
    {
        $this->initialProductionGold = $initialProductionGold;
    
        return $this;
    }

    /**
     * Get initialProductionGold
     *
     * @return integer 
     */
    public function getInitialProductionGold()
    {
        return $this->initialProductionGold;
    }

    /**
     * Set initialProductionBuilding
     *
     * @param integer $initialProductionBuilding
     * @return Game
     */
    public function setInitialProductionBuilding($initialProductionBuilding)
    {
        $this->initialProductionBuilding = $initialProductionBuilding;
    
        return $this;
    }

    /**
     * Get initialProductionBuilding
     *
     * @return integer 
     */
    public function getInitialProductionBuilding()
    {
        return $this->initialProductionBuilding;
    }

    /**
     * Set buildingsCost
     *
     * @param integer $buildingsCost
     * @return Game
     */
    public function setBuildingsCost($buildingsCost)
    {
        $this->buildingsCost = $buildingsCost;
    
        return $this;
    }

    /**
     * Get buildingsCost
     *
     * @return integer 
     */
    public function getBuildingsCost()
    {
        return $this->buildingsCost;
    }

    /**
     * Set buildingsQueueLength
     *
     * @param integer $buildingsQueueLength
     * @return Game
     */
    public function setBuildingsQueueLength($buildingsQueueLength)
    {
        $this->buildingsQueueLength = $buildingsQueueLength;
    
        return $this;
    }

    /**
     * Get buildingsQueueLength
     *
     * @return integer 
     */
    public function getBuildingsQueueLength()
    {
        return $this->buildingsQueueLength;
    }

    /**
     * Set buildingsSpeed
     *
     * @param integer $buildingsSpeed
     * @return Game
     */
    public function setBuildingsSpeed($buildingsSpeed)
    {
        $this->buildingsSpeed = $buildingsSpeed;
    
        return $this;
    }

    /**
     * Get buildingsSpeed
     *
     * @return integer 
     */
    public function getBuildingsSpeed()
    {
        return $this->buildingsSpeed;
    }

    /**
     * Set productionRateFood
     *
     * @param float $productionRateFood
     * @return Game
     */
    public function setProductionRateFood($productionRateFood)
    {
        $this->productionRateFood = $productionRateFood;
    
        return $this;
    }

    /**
     * Get productionRateFood
     *
     * @return float 
     */
    public function getProductionRateFood()
    {
        return $this->productionRateFood;
    }

    /**
     * Set productionRateWood
     *
     * @param float $productionRateWood
     * @return Game
     */
    public function setProductionRateWood($productionRateWood)
    {
        $this->productionRateWood = $productionRateWood;
    
        return $this;
    }

    /**
     * Get productionRateWood
     *
     * @return float 
     */
    public function getProductionRateWood()
    {
        return $this->productionRateWood;
    }

    /**
     * Set productionRateRock
     *
     * @param float $productionRateRock
     * @return Game
     */
    public function setProductionRateRock($productionRateRock)
    {
        $this->productionRateRock = $productionRateRock;
    
        return $this;
    }

    /**
     * Get productionRateRock
     *
     * @return float 
     */
    public function getProductionRateRock()
    {
        return $this->productionRateRock;
    }

    /**
     * Set productionRateGold
     *
     * @param float $productionRateGold
     * @return Game
     */
    public function setProductionRateGold($productionRateGold)
    {
        $this->productionRateGold = $productionRateGold;
    
        return $this;
    }

    /**
     * Get productionRateGold
     *
     * @return float 
     */
    public function getProductionRateGold()
    {
        return $this->productionRateGold;
    }

    /**
     * Set productionRateMadrens
     *
     * @param float $productionRateMadrens
     * @return Game
     */
    public function setProductionRateMadrens($productionRateMadrens)
    {
        $this->productionRateMadrens = $productionRateMadrens;
    
        return $this;
    }

    /**
     * Get productionRateMadrens
     *
     * @return float 
     */
    public function getProductionRateMadrens()
    {
        return $this->productionRateMadrens;
    }

    /**
     * Set researchesCost
     *
     * @param integer $researchesCost
     * @return Game
     */
    public function setResearchesCost($researchesCost)
    {
        $this->researchesCost = $researchesCost;
    
        return $this;
    }

    /**
     * Get researchesCost
     *
     * @return integer 
     */
    public function getResearchesCost()
    {
        return $this->researchesCost;
    }

    /**
     * Set researchesSpeed
     *
     * @param integer $researchesSpeed
     * @return Game
     */
    public function setResearchesSpeed($researchesSpeed)
    {
        $this->researchesSpeed = $researchesSpeed;
    
        return $this;
    }

    /**
     * Get researchesSpeed
     *
     * @return integer 
     */
    public function getResearchesSpeed()
    {
        return $this->researchesSpeed;
    }

    /**
     * Set unitsCapacity
     *
     * @param integer $unitsCapacity
     * @return Game
     */
    public function setUnitsCapacity($unitsCapacity)
    {
        $this->unitsCapacity = $unitsCapacity;
    
        return $this;
    }

    /**
     * Get unitsCapacity
     *
     * @return integer 
     */
    public function getUnitsCapacity()
    {
        return $this->unitsCapacity;
    }

    /**
     * Set unitsCost
     *
     * @param integer $unitsCost
     * @return Game
     */
    public function setUnitsCost($unitsCost)
    {
        $this->unitsCost = $unitsCost;
    
        return $this;
    }

    /**
     * Get unitsCost
     *
     * @return integer 
     */
    public function getUnitsCost()
    {
        return $this->unitsCost;
    }

    /**
     * Set unitsMaintenance
     *
     * @param integer $unitsMaintenance
     * @return Game
     */
    public function setUnitsMaintenance($unitsMaintenance)
    {
        $this->unitsMaintenance = $unitsMaintenance;
    
        return $this;
    }

    /**
     * Get unitsMaintenance
     *
     * @return integer 
     */
    public function getUnitsMaintenance()
    {
        return $this->unitsMaintenance;
    }

    /**
     * Set unitsPoints
     *
     * @param integer $unitsPoints
     * @return Game
     */
    public function setUnitsPoints($unitsPoints)
    {
        $this->unitsPoints = $unitsPoints;
    
        return $this;
    }

    /**
     * Get unitsPoints
     *
     * @return integer 
     */
    public function getUnitsPoints()
    {
        return $this->unitsPoints;
    }

    /**
     * Set unitsSpeed
     *
     * @param integer $unitsSpeed
     * @return Game
     */
    public function setUnitsSpeed($unitsSpeed)
    {
        $this->unitsSpeed = $unitsSpeed;
    
        return $this;
    }

    /**
     * Get unitsSpeed
     *
     * @return integer 
     */
    public function getUnitsSpeed()
    {
        return $this->unitsSpeed;
    }
    /**
     * @var integer
     */
    private $initialResourcesGold;


    /**
     * Set initialResourcesGold
     *
     * @param integer $initialResourcesGold
     * @return Game
     */
    public function setInitialResourcesGold($initialResourcesGold)
    {
        $this->initialResourcesGold = $initialResourcesGold;
    
        return $this;
    }

    /**
     * Get initialResourcesGold
     *
     * @return integer 
     */
    public function getInitialResourcesGold()
    {
        return $this->initialResourcesGold;
    }
    /**
     * @var integer
     */
    private $initialResourcesArpents;


    /**
     * Set initialResourcesArpents
     *
     * @param integer $initialResourcesArpents
     * @return Game
     */
    public function setInitialResourcesArpents($initialResourcesArpents)
    {
        $this->initialResourcesArpents = $initialResourcesArpents;
    
        return $this;
    }

    /**
     * Get initialResourcesArpents
     *
     * @return integer 
     */
    public function getInitialResourcesArpents()
    {
        return $this->initialResourcesArpents;
    }
    
    public function __toString()
    {
        return strval($this->getID());
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $characters;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->characters = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $initialBuilding;


    /**
     * Add initialBuilding
     *
     * @param \LLDC\Bundle\Entity\GameInitialBuilding $initialBuilding
     * @return Game
     */
    public function addInitialBuilding(\LLDC\Bundle\Entity\GameInitialBuilding $initialBuilding)
    {
        $this->initialBuilding[] = $initialBuilding;
    
        return $this;
    }

    /**
     * Remove initialBuilding
     *
     * @param \LLDC\Bundle\Entity\GameInitialBuilding $initialBuilding
     */
    public function removeInitialBuilding(\LLDC\Bundle\Entity\GameInitialBuilding $initialBuilding)
    {
        $this->initialBuilding->removeElement($initialBuilding);
    }

    /**
     * Get initialBuilding
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInitialBuilding()
    {
        return $this->initialBuilding;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $initialTroop;


    /**
     * Add initialTroop
     *
     * @param \LLDC\Bundle\Entity\GameInitialTroop $initialTroop
     * @return Game
     */
    public function addInitialTroop(\LLDC\Bundle\Entity\GameInitialTroop $initialTroop)
    {
        $this->initialTroop[] = $initialTroop;
    
        return $this;
    }

    /**
     * Remove initialTroop
     *
     * @param \LLDC\Bundle\Entity\GameInitialTroop $initialTroop
     */
    public function removeInitialTroop(\LLDC\Bundle\Entity\GameInitialTroop $initialTroop)
    {
        $this->initialTroop->removeElement($initialTroop);
    }

    /**
     * Get initialTroop
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInitialTroop()
    {
        return $this->initialTroop;
    }

    /**
     * @var integer
     */
    private $unitsMaintenanceFrequency;

    /**
     * @var float
     */
    private $unitsMaintenanceDuringInternalWar;

    /**
     * @var float
     */
    private $unitsMaintenanceDuringExternalWar;

    /**
     * Set unitsMaintenanceFrequency
     *
     * @param integer $unitsMaintenanceFrequency
     * @return Game
     */
    public function setUnitsMaintenanceFrequency($unitsMaintenanceFrequency)
    {
        $this->unitsMaintenanceFrequency = $unitsMaintenanceFrequency;

        return $this;
    }

    /**
     * Get unitsMaintenanceFrequency
     *
     * @return integer 
     */
    public function getUnitsMaintenanceFrequency()
    {
        return $this->unitsMaintenanceFrequency;
    }

    /**
     * Set unitsMaintenanceDuringInternalWar
     *
     * @param float $unitsMaintenanceDuringInternalWar
     * @return Game
     */
    public function setUnitsMaintenanceDuringInternalWar($unitsMaintenanceDuringInternalWar)
    {
        $this->unitsMaintenanceDuringInternalWar = $unitsMaintenanceDuringInternalWar;

        return $this;
    }

    /**
     * Get unitsMaintenanceDuringInternalWar
     *
     * @return float 
     */
    public function getUnitsMaintenanceDuringInternalWar()
    {
        return $this->unitsMaintenanceDuringInternalWar;
    }

    /**
     * Set unitsMaintenanceDuringExternalWar
     *
     * @param float $unitsMaintenanceDuringExternalWar
     * @return Game
     */
    public function setUnitsMaintenanceDuringExternalWar($unitsMaintenanceDuringExternalWar)
    {
        $this->unitsMaintenanceDuringExternalWar = $unitsMaintenanceDuringExternalWar;

        return $this;
    }

    /**
     * Get unitsMaintenanceDuringExternalWar
     *
     * @return float 
     */
    public function getUnitsMaintenanceDuringExternalWar()
    {
        return $this->unitsMaintenanceDuringExternalWar;
    }
    /**
     * @var integer
     */
    private $unitsMaintenanceMoraleLoss;


    /**
     * Set unitsMaintenanceMoraleLoss
     *
     * @param integer $unitsMaintenanceMoraleLoss
     * @return Game
     */
    public function setUnitsMaintenanceMoraleLoss($unitsMaintenanceMoraleLoss)
    {
        $this->unitsMaintenanceMoraleLoss = $unitsMaintenanceMoraleLoss;

        return $this;
    }

    /**
     * Get unitsMaintenanceMoraleLoss
     *
     * @return integer 
     */
    public function getUnitsMaintenanceMoraleLoss()
    {
        return $this->unitsMaintenanceMoraleLoss;
    }
    /**
     * @var integer
     */
    private $initialResourcesMorale;


    /**
     * Set initialResourcesMorale
     *
     * @param integer $initialResourcesMorale
     * @return Game
     */
    public function setInitialResourcesMorale($initialResourcesMorale)
    {
        $this->initialResourcesMorale = $initialResourcesMorale;

        return $this;
    }

    /**
     * Get initialResourcesMorale
     *
     * @return integer 
     */
    public function getInitialResourcesMorale()
    {
        return $this->initialResourcesMorale;
    }
    /**
     * @var integer
     */
    private $battlefieldsSize;

    /**
     * @var integer
     */
    private $battlefieldsMaxAmount;


    /**
     * Set battlefieldsSize
     *
     * @param integer $battlefieldsSize
     * @return Game
     */
    public function setBattlefieldsSize($battlefieldsSize)
    {
        $this->battlefieldsSize = $battlefieldsSize;

        return $this;
    }

    /**
     * Get battlefieldsSize
     *
     * @return integer 
     */
    public function getBattlefieldsSize()
    {
        return $this->battlefieldsSize;
    }

    /**
     * Set battlefieldsMaxAmount
     *
     * @param integer $battlefieldsMaxAmount
     * @return Game
     */
    public function setBattlefieldsMaxAmount($battlefieldsMaxAmount)
    {
        $this->battlefieldsMaxAmount = $battlefieldsMaxAmount;

        return $this;
    }

    /**
     * Get battlefieldsMaxAmount
     *
     * @return integer 
     */
    public function getBattlefieldsMaxAmount()
    {
        return $this->battlefieldsMaxAmount;
    }
    /**
     * @var float
     */
    private $unitsMaintenanceFactor;


    /**
     * Set unitsMaintenanceFactor
     *
     * @param float $unitsMaintenanceFactor
     * @return Game
     */
    public function setUnitsMaintenanceFactor($unitsMaintenanceFactor)
    {
        $this->unitsMaintenanceFactor = $unitsMaintenanceFactor;
        return $this;

    }
    /**
     * Get unitsMaintenanceFactor
     *
     * @return float 
     */
    public function getUnitsMaintenanceFactor()
    {
        return $this->unitsMaintenanceFactor;
    }

    private $initialResourcesXp;

    /**
     * Set initialResourcesXp
     *
     * @param float $initialResourcesXp
     * @return Game
     */
    public function setInitialResourcesXp($initialResourcesXp)
    {
        $this->initialResourcesXp = $initialResourcesXp;
        return $this;
    }
    /*
     * Get initialResourcesXp
     *
     * @return float 
     */
    public function getInitialResourcesXp()
    {
        return $this->initialResourcesXp;
    }
}
