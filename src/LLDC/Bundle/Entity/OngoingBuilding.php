<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OngoingBuilding
 */
class OngoingBuilding
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $building;

    /**
     * @var decimal
     */
    private $progression;

    /**
     * @var \DateTime
     */
    private $dateBegin;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set building
     *
     * @param string $building
     * @return OngoingBuilding
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    
        return $this;
    }

    /**
     * Get building
     *
     * @return string 
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set progression
     *
     * @param decimal $progression
     * @return OngoingBuilding
     */
    public function setProgression($progression)
    {
        $this->progression = $progression;
    
        return $this;
    }

    /**
     * Get progression
     *
     * @return decimal 
     */
    public function getProgression()
    {
        return $this->progression;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     * @return OngoingBuilding
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;
    
        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime 
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return OngoingBuilding
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;
    
        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm 
     */
    public function getRealm()
    {
        return $this->realm;
    }
    /**
     * @var integer
     */
    private $duration;


    /**
     * Set duration
     *
     * @param integer $duration
     * @return OngoingBuilding
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }
    /**
     * @var \DateTime
     */
    private $dateLastWork;


    /**
     * Set dateLastWork
     *
     * @param \DateTime $dateLastWork
     * @return OngoingBuilding
     */
    public function setDateLastWork($dateLastWork)
    {
        $this->dateLastWork = $dateLastWork;
    
        return $this;
    }

    /**
     * Get dateLastWork
     *
     * @return \DateTime 
     */
    public function getDateLastWork()
    {
        return $this->dateLastWork;
    }
    /**
     * @var \DateTime
     */
    private $dateEstimatedEnd;


    /**
     * Set dateEstimatedEnd
     *
     * @param \DateTime $dateEstimatedEnd
     * @return OngoingBuilding
     */
    public function setDateEstimatedEnd($dateEstimatedEnd)
    {
        $this->dateEstimatedEnd = $dateEstimatedEnd;

        return $this;
    }

    /**
     * Get dateEstimatedEnd
     *
     * @return \DateTime
     */
    public function getDateEstimatedEnd()
    {
        return $this->dateEstimatedEnd;
    }
    /**
     * @var string
     */
    private $type;


    /**
     * Set type
     *
     * @param string $type
     * @return OngoingBuilding
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
