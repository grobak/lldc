<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OngoingResearch
 */
class OngoingResearch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $research;

    /**
     * @var \DateTime
     */
    private $dateBegin;

    /**
     * @var \DateTime
     */
    private $dateEnd;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set research
     *
     * @param string $research
     * @return OngoingResearch
     */
    public function setResearch($research)
    {
        $this->research = $research;
    
        return $this;
    }

    /**
     * Get research
     *
     * @return string 
     */
    public function getResearch()
    {
        return $this->research;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     * @return OngoingResearch
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;
    
        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime 
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return OngoingResearch
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return OngoingResearch
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;
    
        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm 
     */
    public function getRealm()
    {
        return $this->realm;
    }
}
