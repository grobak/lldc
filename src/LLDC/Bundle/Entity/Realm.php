<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Realm
 */
class Realm implements \JsonSerializable
{
    /**
     * JSon serialization
     */
    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'character' => $this->getCharacter(),
            'place' => $this->getPlace()
        );
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @var \LLDC\Bundle\Entity\User
     */
    private $user;

    /**
     * @var \LLDC\Bundle\Entity\Game
     */
    private $game;

    /**
     * @var \LLDC\Bundle\Entity\RealmProduction
     */
    private $production;

    /**
     * @var \LLDC\Bundle\Entity\RealmResource
     */
    private $resource;

    /**
     * @var \LLDC\Bundle\Entity\RealmBuilding
     */
    private $building;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Realm
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set user
     *
     * @param \LLDC\Bundle\Entity\User $user
     * @return Realm
     */
    public function setUser(\LLDC\Bundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \LLDC\Bundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set game
     *
     * @param \LLDC\Bundle\Entity\Game $game
     * @return Realm
     */
    public function setGame(\LLDC\Bundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \LLDC\Bundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set production
     *
     * @param \LLDC\Bundle\Entity\RealmProduction $production
     * @return Realm
     */
    public function setProduction(\LLDC\Bundle\Entity\RealmProduction $production = null)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * Get production
     *
     * @return \LLDC\Bundle\Entity\RealmProduction
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Set resource
     *
     * @param \LLDC\Bundle\Entity\RealmResource $resource
     * @return Realm
     */
    public function setResource(\LLDC\Bundle\Entity\RealmResource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return \LLDC\Bundle\Entity\RealmResource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Set building
     *
     * @param \LLDC\Bundle\Entity\RealmBuilding $building
     * @return Realm
     */
    public function setBuilding(\LLDC\Bundle\Entity\RealmBuilding $building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return \LLDC\Bundle\Entity\RealmBuilding
     */
    public function getBuilding()
    {
        return $this->building;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $researches;

    /**
     * Add researches
     *
     * @param \LLDC\Bundle\Entity\RealmResearch $researches
     * @return Realm
     */
    public function addResearche(\LLDC\Bundle\Entity\RealmResearch $researches)
    {
        $this->researches[] = $researches;

        return $this;
    }

    /**
     * Remove researches
     *
     * @param \LLDC\Bundle\Entity\RealmResearch $researches
     */
    public function removeResearche(\LLDC\Bundle\Entity\RealmResearch $researches)
    {
        $this->researches->removeElement($researches);
    }

    /**
     * Get researches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResearches()
    {
        return $this->researches;
    }

    /**
     * @var \LLDC\Bundle\Entity\OngoingResearch
     */
    private $ongoingResearch;


    /**
     * Set ongoingResearch
     *
     * @param \LLDC\Bundle\Entity\OngoingResearch $ongoingResearch
     * @return Realm
     */
    public function setOngoingResearch(\LLDC\Bundle\Entity\OngoingResearch $ongoingResearch = null)
    {
        $this->ongoingResearch = $ongoingResearch;

        return $this;
    }

    /**
     * Get ongoingResearch
     *
     * @return \LLDC\Bundle\Entity\OngoingResearch
     */
    public function getOngoingResearch()
    {
        return $this->ongoingResearch;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ongoingBuilding;


    /**
     * Add ongoingBuilding
     *
     * @param \LLDC\Bundle\Entity\OngoingBuilding $ongoingBuilding
     * @return Realm
     */
    public function addOngoingBuilding(\LLDC\Bundle\Entity\OngoingBuilding $ongoingBuilding)
    {
        $this->ongoingBuilding[] = $ongoingBuilding;
    
        return $this;
    }

    /**
     * Remove ongoingBuilding
     *
     * @param \LLDC\Bundle\Entity\OngoingBuilding $ongoingBuilding
     */
    public function removeOngoingBuilding(\LLDC\Bundle\Entity\OngoingBuilding $ongoingBuilding)
    {
        $this->ongoingBuilding->removeElement($ongoingBuilding);
    }

    /**
     * Get ongoingBuilding
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOngoingBuilding()
    {
        return $this->ongoingBuilding;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $notification;


    /**
     * Add notification
     *
     * @param \LLDC\Bundle\Entity\Notification $notification
     * @return Realm
     */
    public function addNotification(\LLDC\Bundle\Entity\Notification $notification)
    {
        $this->notification[] = $notification;
    
        return $this;
    }

    /**
     * Remove notification
     *
     * @param \LLDC\Bundle\Entity\Notification $notification
     */
    public function removeNotification(\LLDC\Bundle\Entity\Notification $notification)
    {
        $this->notification->removeElement($notification);
    }

    /**
     * Get notification
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotification()
    {
        return $this->notification;
    }
    /**
     * @var \LLDC\Bundle\Entity\RpCharacter
     */
    private $character;

    /**
     * @var \LLDC\Bundle\Entity\RpPlace
     */
    private $place;


    /**
     * Set character
     *
     * @param \LLDC\Bundle\Entity\RpCharacter $character
     * @return Realm
     */
    public function setCharacter(\LLDC\Bundle\Entity\RpCharacter $character = null)
    {
        $this->character = $character;
    
        return $this;
    }

    /**
     * Get character
     *
     * @return \LLDC\Bundle\Entity\RpCharacter 
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * Set place
     *
     * @param \LLDC\Bundle\Entity\RpPlace $place
     * @return Realm
     */
    public function setPlace(\LLDC\Bundle\Entity\RpPlace $place = null)
    {
        $this->place = $place;
    
        return $this;
    }

    /**
     * Get place
     *
     * @return \LLDC\Bundle\Entity\RpPlace 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Add building
     *
     * @param \LLDC\Bundle\Entity\RealmBuilding $building
     * @return Realm
     */
    public function addBuilding(\LLDC\Bundle\Entity\RealmBuilding $building)
    {
        $this->building[] = $building;
    
        return $this;
    }

    /**
     * Remove building
     *
     * @param \LLDC\Bundle\Entity\RealmBuilding $building
     */
    public function removeBuilding(\LLDC\Bundle\Entity\RealmBuilding $building)
    {
        $this->building->removeElement($building);
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attacker;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $defender;


    /**
     * Add attacker
     *
     * @param \LLDC\Bundle\Entity\War $attacker
     * @return Realm
     */
    public function addAttacker(\LLDC\Bundle\Entity\War $attacker)
    {
        $this->attacker[] = $attacker;
    
        return $this;
    }

    /**
     * Remove attacker
     *
     * @param \LLDC\Bundle\Entity\War $attacker
     */
    public function removeAttacker(\LLDC\Bundle\Entity\War $attacker)
    {
        $this->attacker->removeElement($attacker);
    }

    /**
     * Get attacker
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttacker()
    {
        return $this->attacker;
    }

    /**
     * Add defender
     *
     * @param \LLDC\Bundle\Entity\War $defender
     * @return Realm
     */
    public function addDefender(\LLDC\Bundle\Entity\War $defender)
    {
        $this->defender[] = $defender;
    
        return $this;
    }

    /**
     * Remove defender
     *
     * @param \LLDC\Bundle\Entity\War $defender
     */
    public function removeDefender(\LLDC\Bundle\Entity\War $defender)
    {
        $this->defender->removeElement($defender);
    }

    /**
     * Get defender
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDefender()
    {
        return $this->defender;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attacking;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $defending;


    /**
     * Add attacking
     *
     * @param \LLDC\Bundle\Entity\War $attacking
     * @return Realm
     */
    public function addAttacking(\LLDC\Bundle\Entity\War $attacking)
    {
        $this->attacking[] = $attacking;
    
        return $this;
    }

    /**
     * Remove attacking
     *
     * @param \LLDC\Bundle\Entity\War $attacking
     */
    public function removeAttacking(\LLDC\Bundle\Entity\War $attacking)
    {
        $this->attacking->removeElement($attacking);
    }

    /**
     * Get attacking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttacking()
    {
        return $this->attacking;
    }

    /**
     * Add defending
     *
     * @param \LLDC\Bundle\Entity\War $defending
     * @return Realm
     */
    public function addDefending(\LLDC\Bundle\Entity\War $defending)
    {
        $this->defending[] = $defending;
    
        return $this;
    }

    /**
     * Remove defending
     *
     * @param \LLDC\Bundle\Entity\War $defending
     */
    public function removeDefending(\LLDC\Bundle\Entity\War $defending)
    {
        $this->defending->removeElement($defending);
    }

    /**
     * Get defending
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDefending()
    {
        return $this->defending;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $troop;


    /**
     * Add troop
     *
     * @param \LLDC\Bundle\Entity\Troop $troop
     * @return Realm
     */
    public function addTroop(\LLDC\Bundle\Entity\Troop $troop)
    {
        $this->troop[] = $troop;
    
        return $this;
    }

    /**
     * Remove troop
     *
     * @param \LLDC\Bundle\Entity\Troop $troop
     */
    public function removeTroop(\LLDC\Bundle\Entity\Troop $troop)
    {
        $this->troop->removeElement($troop);
    }

    /**
     * Get troop
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTroop()
    {
        return $this->troop;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ranking;


    /**
     * Add researches
     *
     * @param \LLDC\Bundle\Entity\RealmResearch $researches
     * @return Realm
     */
    public function addResearch(\LLDC\Bundle\Entity\RealmResearch $researches)
    {
        $this->researches[] = $researches;

        return $this;
    }

    /**
     * Remove researches
     *
     * @param \LLDC\Bundle\Entity\RealmResearch $researches
     */
    public function removeResearch(\LLDC\Bundle\Entity\RealmResearch $researches)
    {
        $this->researches->removeElement($researches);
    }

    /**
     * Add ranking
     *
     * @param \LLDC\Bundle\Entity\RealmRanking $ranking
     * @return Realm
     */
    public function addRanking(\LLDC\Bundle\Entity\RealmRanking $ranking)
    {
        $this->ranking[] = $ranking;

        return $this;
    }

    /**
     * Remove ranking
     *
     * @param \LLDC\Bundle\Entity\RealmRanking $ranking
     */
    public function removeRanking(\LLDC\Bundle\Entity\RealmRanking $ranking)
    {
        $this->ranking->removeElement($ranking);
    }

    /**
     * Get ranking
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRanking()
    {
        return $this->ranking;
    }
}
