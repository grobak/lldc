<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealmBuilding
 */
class RealmBuilding
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $house;

    /**
     * @var integer
     */
    private $market;

    /**
     * @var integer
     */
    private $spycenter;

    /**
     * @var integer
     */
    private $barrack;

    /**
     * @var integer
     */
    private $archery;

    /**
     * @var integer
     */
    private $stable;

    /**
     * @var integer
     */
    private $temple;

    /**
     * @var integer
     */
    private $special;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set house
     *
     * @param integer $house
     * @return RealmBuilding
     */
    public function setHouse($house)
    {
        $this->house = $house>0 ? $house : 0;

        return $this;
    }

    /**
     * Get house
     *
     * @return integer
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set market
     *
     * @param integer $market
     * @return RealmBuilding
     */
    public function setMarket($market)
    {
        $this->market = $market>0 ? $market : 0;

        return $this;
    }

    /**
     * Get market
     *
     * @return integer
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * Set spycenter
     *
     * @param integer $spycenter
     * @return RealmBuilding
     */
    public function setSpycenter($spycenter)
    {
        $this->spycenter = $spycenter>0 ? $spycenter : 0;

        return $this;
    }

    /**
     * Get spycenter
     *
     * @return integer
     */
    public function getSpycenter()
    {
        return $this->spycenter;
    }

    /**
     * Set barrack
     *
     * @param integer $barrack
     * @return RealmBuilding
     */
    public function setBarrack($barrack)
    {
        $this->barrack = $barrack>0 ? $barrack : 0;

        return $this;
    }

    /**
     * Get barrack
     *
     * @return integer
     */
    public function getBarrack()
    {
        return $this->barrack;
    }

    /**
     * Set archery
     *
     * @param integer $archery
     * @return RealmBuilding
     */
    public function setArchery($archery)
    {
        $this->archery = $archery>0 ? $archery : 0;

        return $this;
    }

    /**
     * Get archery
     *
     * @return integer
     */
    public function getArchery()
    {
        return $this->archery;
    }

    /**
     * Set stable
     *
     * @param integer $stable
     * @return RealmBuilding
     */
    public function setStable($stable)
    {
        $this->stable = $stable>0 ? $stable : 0;

        return $this;
    }

    /**
     * Get stable
     *
     * @return integer
     */
    public function getStable()
    {
        return $this->stable;
    }

    /**
     * Set temple
     *
     * @param integer $temple
     * @return RealmBuilding
     */
    public function setTemple($temple)
    {
        $this->temple = $temple>0 ? $temple : 0;

        return $this;
    }

    /**
     * Get temple
     *
     * @return integer
     */
    public function getTemple()
    {
        return $this->temple;
    }

    /**
     * Set special
     *
     * @param integer $special
     * @return RealmBuilding
     */
    public function setSpecial($special)
    {
        $this->special = $special>0 ? $special : 0;

        return $this;
    }

    /**
     * Get special
     *
     * @return integer
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return RealmBuilding
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm
     */
    public function getRealm()
    {
        return $this->realm;
    }
    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $amount;


    /**
     * Set type
     *
     * @param string $type
     * @return RealmBuilding
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return RealmBuilding
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
