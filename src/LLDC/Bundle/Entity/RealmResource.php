<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealmResource
 */
class RealmResource
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $food;

    /**
     * @var integer
     */
    private $wood;

    /**
     * @var integer
     */
    private $rock;

    /**
     * @var integer
     */
    private $gold;

    /**
     * @var integer
     */
    private $madrens;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set food
     *
     * @param integer $food
     * @return RealmResource
     */
    public function setFood($food)
    {
        $this->food = $food;

        return $this;
    }

    /**
     * Get food
     *
     * @return integer
     */
    public function getFood()
    {
        return $this->food;
    }

    /**
     * Set wood
     *
     * @param integer $wood
     * @return RealmResource
     */
    public function setWood($wood)
    {
        $this->wood = $wood;

        return $this;
    }

    /**
     * Get wood
     *
     * @return integer
     */
    public function getWood()
    {
        return $this->wood;
    }

    /**
     * Set rock
     *
     * @param integer $rock
     * @return RealmResource
     */
    public function setRock($rock)
    {
        $this->rock = $rock;

        return $this;
    }

    /**
     * Get rock
     *
     * @return integer
     */
    public function getRock()
    {
        return $this->rock;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     * @return RealmResource
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get gold
     *
     * @return integer
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * Set madrens
     *
     * @param integer $madrens
     * @return RealmResource
     */
    public function setMadrens($madrens)
    {
        $this->madrens = $madrens;

        return $this;
    }

    /**
     * Get madrens
     *
     * @return integer
     */
    public function getMadrens()
    {
        return $this->madrens;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return RealmResource
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm
     */
    public function getRealm()
    {
        return $this->realm;
    }
    /**
     * @var integer
     */
    private $morale;


    /**
     * Set morale
     *
     * @param integer $morale
     * @return RealmResource
     */
    public function setMorale($morale)
    {
        $this->morale = $morale;

        return $this;
    }

    /**
     * Get morale
     *
     * @return integer 
     */
    public function getMorale()
    {
        return $this->morale;
    }
    /**
     * @var float
     */
    private $xp;


    /**
     * Set xp
     *
     * @param float $xp
     * @return RealmResource
     */
    public function setXp($xp)
    {
        $this->xp = $xp;

        return $this;
    }

    /**
     * Get xp
     *
     * @return float 
     */
    public function getXp()
    {
        return $this->xp;
    }
}
