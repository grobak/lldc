<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManager;

/**
 * RpCharacter
 */
class RpCharacter implements \JsonSerializable
{
    /**
     * JSon serialization
     */
    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'name' => $this->name
        );
    }

    public function handleUpload(EntityManager $entityManager, $file = null)
    {
        if (!is_null($file)) {
            // guestExtension instead of getExtension, the second one doesn't work, I don't know why
            $avatar = new Image();
            $avatar->setType($file->guessExtension());

            $stream = fopen($file->getRealPath(), 'rb');
            $avatar->setData(stream_get_contents($stream));
            $avatar->setChecksum(md5($avatar->getData()));

            $entityManager->persist($avatar);
            $this->setAvatar($avatar);
        }
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RpCharacter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return RpCharacter
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;

        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm
     */
    public function getRealm()
    {
        return $this->realm;
    }
    /**
     * @var \LLDC\Bundle\Entity\Race
     */
    private $race;

    /**
     * Set race
     *
     * @param \LLDC\Bundle\Entity\Race $race
     * @return RpCharacter
     */
    public function setRace(\LLDC\Bundle\Entity\Race $race = null)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return \LLDC\Bundle\Entity\Race
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @var \LLDC\Bundle\Entity\Gender
     */
    private $gender;

    /**
     * Set gender
     *
     * @param \LLDC\Bundle\Entity\Gender $gender
     * @return RpCharacter
     */
    public function setGender(\LLDC\Bundle\Entity\Gender $gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return \LLDC\Bundle\Entity\Gender
     */
    public function getGender()
    {
        return $this->gender;
    }
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $story;

    /**
     * @var int
     */
    private $age;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $height;

    /**
     * @var string
     */
    private $weight;

    /**
     * @var string
     */
    private $physiognomy;

    /**
     * @var string
     */
    private $hairs;

    /**
     * @var string
     */
    private $eyes;

    /**
     * @var string
     */
    private $dressStyle;

    /**
     * @var string
     */
    private $distinctiveSign;

    /**
     * @var string
     */
    private $tastes;

    /**
     * @var string
     */
    private $favoriteObject;

    /**
     * @var string
     */
    private $companion;

    /**
     * @var string
     */
    private $mainWeapon;

    /**
     * @var string
     */
    private $secondaryWeapon;

    /**
     * @var string
     */
    private $armor;

    /**
     * @var string
     */
    private $power;

    /**
     * @var string
     */
    private $talent;

    /**
     * @var string
     */
    private $weakness;

    /**
     * @var string
     */
    private $conveyance;

    /**
     * @var string
     */
    private $inventory;

    /**
     * @var \DateTime
     */
    private $dateUploadPicture;

    /**
     * @var boolean
     */
    private $defaultForum;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var boolean
     */
    private $displayed;

    /**
     * Set title
     *
     * @param string $title
     * @return RpCharacter
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set story
     *
     * @param string $story
     * @return RpCharacter
     */
    public function setStory($story)
    {
        $this->story = $story;
        return $this;
    }

    /**
     * Get story
     *
     * @return string
     */
    public function getStory()
    {
        return $this->story;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return RpCharacter
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return RpCharacter
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return RpCharacter
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set height
     *
     * @param string $height
     * @return RpCharacter
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return RpCharacter
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set physiognomy
     *
     * @param string $physiognomy
     * @return RpCharacter
     */
    public function setPhysiognomy($physiognomy)
    {
        $this->physiognomy = $physiognomy;
        return $this;
    }

    /**
     * Get physiognomy
     *
     * @return string
     */
    public function getPhysiognomy()
    {
        return $this->physiognomy;
    }

    /**
     * Set hairs
     *
     * @param string $hairs
     * @return RpCharacter
     */
    public function setHairs($hairs)
    {
        $this->hairs = $hairs;
        return $this;
    }

    /**
     * Get hairs
     *
     * @return string
     */
    public function getHairs()
    {
        return $this->hairs;
    }

    /**
     * Set eyes
     *
     * @param string $eyes
     * @return RpCharacter
     */
    public function setEyes($eyes)
    {
        $this->eyes = $eyes;
        return $this;
    }

    /**
     * Get eyes
     *
     * @return string
     */
    public function getEyes()
    {
        return $this->eyes;
    }

    /**
     * Set dressStyle
     *
     * @param string $dressStyle
     * @return RpCharacter
     */
    public function setDressStyle($dressStyle)
    {
        $this->dressStyle = $dressStyle;
        return $this;
    }

    /**
     * Get dressStyle
     *
     * @return string
     */
    public function getDressStyle()
    {
        return $this->dressStyle;
    }

    /**
     * Set distinctiveSign
     *
     * @param string $distinctiveSign
     * @return RpCharacter
     */
    public function setDistinctiveSign($distinctiveSign)
    {
        $this->distinctiveSign = $distinctiveSign;
        return $this;
    }

    /**
     * Get distinctiveSign
     *
     * @return string
     */
    public function getDistinctiveSign()
    {
        return $this->distinctiveSign;
    }

    /**
     * Set tastes
     *
     * @param string $tastes
     * @return RpCharacter
     */
    public function setTastes($tastes)
    {
        $this->tastes = $tastes;
        return $this;
    }

    /**
     * Get tastes
     *
     * @return string
     */
    public function getTastes()
    {
        return $this->tastes;
    }

    /**
     * Set favoriteObject
     *
     * @param string $favoriteObject
     * @return RpCharacter
     */
    public function setFavoriteObject($favoriteObject)
    {
        $this->favoriteObject = $favoriteObject;
        return $this;
    }

    /**
     * Get favoriteObject
     *
     * @return string
     */
    public function getFavoriteObject()
    {
        return $this->favoriteObject;
    }

    /**
     * Set companion
     *
     * @param string $companion
     * @return RpCharacter
     */
    public function setCompanion($companion)
    {
        $this->companion = $companion;
        return $this;
    }

    /**
     * Get companion
     *
     * @return string
     */
    public function getCompanion()
    {
        return $this->companion;
    }

    /**
     * Set mainWeapon
     *
     * @param string $mainWeapon
     * @return RpCharacter
     */
    public function setMainWeapon($mainWeapon)
    {
        $this->mainWeapon = $mainWeapon;
        return $this;
    }

    /**
     * Get mainWeapon
     *
     * @return string
     */
    public function getMainWeapon()
    {
        return $this->mainWeapon;
    }

    /**
     * Set secondaryWeapon
     *
     * @param string $secondaryWeapon
     * @return RpCharacter
     */
    public function setSecondaryWeapon($secondaryWeapon)
    {
        $this->secondaryWeapon = $secondaryWeapon;
        return $this;
    }

    /**
     * Get secondaryWeapon
     *
     * @return string
     */
    public function getSecondaryWeapon()
    {
        return $this->secondaryWeapon;
    }

    /**
     * Set armor
     *
     * @param string $armor
     * @return RpCharacter
     */
    public function setArmor($armor)
    {
        $this->armor = $armor;
        return $this;
    }

    /**
     * Get armor
     *
     * @return string
     */
    public function getArmor()
    {
        return $this->armor;
    }

    /**
     * Set power
     *
     * @param string $power
     * @return RpCharacter
     */
    public function setPower($power)
    {
        $this->power = $power;
        return $this;
    }

    /**
     * Get power
     *
     * @return string
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * Set talent
     *
     * @param string $talent
     * @return RpCharacter
     */
    public function setTalent($talent)
    {
        $this->talent = $talent;
        return $this;
    }

    /**
     * Get talent
     *
     * @return string
     */
    public function getTalent()
    {
        return $this->talent;
    }

    /**
     * Set weakness
     *
     * @param string $weakness
     * @return RpCharacter
     */
    public function setWeakness($weakness)
    {
        $this->weakness = $weakness;
        return $this;
    }

    /**
     * Get weakness
     *
     * @return string
     */
    public function getWeakness()
    {
        return $this->weakness;
    }

    /**
     * Set conveyance
     *
     * @param string $conveyance
     * @return RpCharacter
     */
    public function setConveyance($conveyance)
    {
        $this->conveyance = $conveyance;
        return $this;
    }

    /**
     * Get conveyance
     *
     * @return string
     */
    public function getConveyance()
    {
        return $this->conveyance;
    }

    /**
     * Set inventory
     *
     * @param string $inventory
     * @return RpCharacter
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
        return $this;
    }

    /**
     * Get inventory
     *
     * @return string
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set defaultForum
     *
     * @param boolean $defaultForum
     * @return RpCharacter
     */
    public function setDefaultForum($defaultForum)
    {
        $this->defaultForum = $defaultForum;
        return $this;
    }

    /**
     * Get defaultForum
     *
     * @return boolean
     */
    public function getDefaultForum()
    {
        return $this->defaultForum;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return RpCharacter
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set displayed
     *
     * @param boolean $displayed
     * @return RpCharacter
     */
    public function setDisplayed($displayed)
    {
        $this->displayed = $displayed;
        return $this;
    }

    /**
     * Get displayed
     *
     * @return boolean
     */
    public function getDisplayed()
    {
        return $this->displayed;
    }
    /**
     * @var string
     */
    private $nickname;


    /**
     * Set nickname
     *
     * @param string $nickname
     * @return RpCharacter
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }
    /**
     * @var \LLDC\Bundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \LLDC\Bundle\Entity\User $user
     * @return RpCharacter
     */
    public function setUser(\LLDC\Bundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \LLDC\Bundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @var \LLDC\Bundle\Entity\Image
     */
    private $avatar;


    /**
     * Set avatar
     *
     * @param \LLDC\Bundle\Entity\Image $avatar
     * @return RpCharacter
     */
    public function setAvatar(\LLDC\Bundle\Entity\Image $avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return \LLDC\Bundle\Entity\Image
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

}
