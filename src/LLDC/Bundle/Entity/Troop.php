<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Troop
 */
class Troop implements \JsonSerializable
{
    /**
     * @var array(mixed)
     * This temporary stack is used for saving information, cleared after each turn during a war
     */
    private $tmpStack;
    public function getTmpStack() {
        return $this->tmpStack;
    }
    public function setTmpStack($tmpStack) {
        $this->tmpStack = $tmpStack;
        return $this->tmpStack;
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var \DateTime
     */
    private $dateCreation;

    /**
     * @var \LLDC\Bundle\Entity\Realm
     */
    private $realm;

    /**
     * @var \LLDC\Bundle\Entity\War
     */
    private $war;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Troop
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Troop
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Troop
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set realm
     *
     * @param \LLDC\Bundle\Entity\Realm $realm
     * @return Troop
     */
    public function setRealm(\LLDC\Bundle\Entity\Realm $realm = null)
    {
        $this->realm = $realm;
    
        return $this;
    }

    /**
     * Get realm
     *
     * @return \LLDC\Bundle\Entity\Realm 
     */
    public function getRealm()
    {
        return $this->realm;
    }

    /**
     * Set war
     *
     * @param \LLDC\Bundle\Entity\War $war
     * @return Troop
     */
    public function setWar(\LLDC\Bundle\Entity\War $war = null)
    {
        $this->war = $war;
    
        return $this;
    }

    /**
     * Get war
     *
     * @return \LLDC\Bundle\Entity\War 
     */
    public function getWar()
    {
        return $this->war;
    }

    public function jsonSerialize() {
        $fields = [
            "id"=>$this->getId(),
            "type"=>$this->getType(),
            "amount"=>$this->getAmount(),
            "race"=>$this->getRace()->getLabel(),
            "full_type"=>$this->getFullType()
        ];
        return $fields;
    }
    /**
     * @var \LLDC\Bundle\Entity\Race
     */
    private $race;


    /**
     * Set race
     *
     * @param \LLDC\Bundle\Entity\Race $race
     * @return Troop
     */
    public function setRace(\LLDC\Bundle\Entity\Race $race = null)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return \LLDC\Bundle\Entity\Race
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Get full type
     *
     * @return string
     */
    public function getFullType()
    {
        return $this->race->getLabel().'/'.$this->type;
    }
}
