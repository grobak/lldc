<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser implements \JsonSerializable
{
    public function jsonSerialize() {
        return array(
            'id' => $this->id,
            'username' => $this->username
        );
    }

    /**
     * @var \DateTime
     */
    private $dateRegistration;

    /**
     * @var boolean
     */
    private $isBanned;

    /**
     * @var \DateTime
     */
    private $dateBanUntil;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var \DateTime
     */
    private $dateRestUntil;

    /**
     * @var string
     */
    private $registrationRace;

    /**
     * @var integer
     */
    private $registrationGender;

    /**
     * @var string
     */
    private $registrationIdGuild;

    /**
     * @var string
     */
    private $registrationRealmName;

    /**
     * @var string
     */
    private $locale;

    /**
     *
     */
    public function __construct() {
        parent::__construct();
        $this->realms = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setDateRegistration(new \DateTime());
        $this->setIsBanned(false);
        $this->setIsActive(true);
    }

    /**
     * Set dateRegistration
     *
     * @param \DateTime $dateRegistration
     * @return User
     */
    public function setDateRegistration($dateRegistration)
    {
        $this->dateRegistration = $dateRegistration;

        return $this;
    }

    /**
     * Get dateRegistration
     *
     * @return \DateTime
     */
    public function getDateRegistration()
    {
        return $this->dateRegistration;
    }

    /**
     * Set dateLastConnection
     *
     * @param \DateTime $dateLastConnection
     * @return User
     */
    public function setDateLastConnection($dateLastConnection)
    {
        $this->dateLastConnection = $dateLastConnection;

        return $this;
    }

    /**
     * Get dateLastConnection
     *
     * @return \DateTime
     */
    public function getDateLastConnection()
    {
        return $this->dateLastConnection;
    }

    /**
     * Set isBanned
     *
     * @param boolean $isBanned
     * @return User
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;

        return $this;
    }

    /**
     * Get isBanned
     *
     * @return boolean
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * Set dateBanUntil
     *
     * @param \DateTime $dateBanUntil
     * @return User
     */
    public function setDateBanUntil($dateBanUntil)
    {
        $this->dateBanUntil = $dateBanUntil;

        return $this;
    }

    /**
     * Get dateBanUntil
     *
     * @return \DateTime
     */
    public function getDateBanUntil()
    {
        return $this->dateBanUntil;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set dateRestUntil
     *
     * @param \DateTime $dateRestUntil
     * @return User
     */
    public function setDateRestUntil($dateRestUntil)
    {
        $this->dateRestUntil = $dateRestUntil;

        return $this;
    }

    /**
     * Get dateRestUntil
     *
     * @return \DateTime
     */
    public function getDateRestUntil()
    {
        return $this->dateRestUntil;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return array('ROLE_USER');
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    public function setRegistrationRealmName($realmName)
    {
        $this->registrationRealmName = $realmName;
    }
    public function getRegistrationRealmName()
    {
        return $this->registrationRealmName;
    }

    public function setRegistrationRace($race)
    {
        $this->registrationRace = $race;
    }
    public function getRegistrationRace()
    {
        return $this->registrationRace;
    }

    public function setRegistrationGender($gender)
    {
        $this->registrationGender = $gender;
    }
    public function getRegistrationGender()
    {
        return $this->registrationGender;
    }

    public function setRegistrationIdGuild($idGuild)
    {
        $this->registrationIdGuild = $idGuild;
    }
    public function getRegistrationIdGuild()
    {
        return $this->registrationIdGuild;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $characters;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $places;

    /**
     * Add characters
     *
     * @param \LLDC\Bundle\Entity\RpCharacter $characters
     * @return User
     */
    public function addCharacter(\LLDC\Bundle\Entity\RpCharacter $characters)
    {
        $this->characters[] = $characters;
    
        return $this;
    }

    /**
     * Remove characters
     *
     * @param \LLDC\Bundle\Entity\RpCharacter $characters
     */
    public function removeCharacter(\LLDC\Bundle\Entity\RpCharacter $characters)
    {
        $this->characters->removeElement($characters);
    }

    /**
     * Get characters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * Add places
     *
     * @param \LLDC\Bundle\Entity\RpPlace $places
     * @return User
     */
    public function addPlace(\LLDC\Bundle\Entity\RpPlace $places)
    {
        $this->places[] = $places;
    
        return $this;
    }

    /**
     * Remove places
     *
     * @param \LLDC\Bundle\Entity\RpPlace $places
     */
    public function removePlace(\LLDC\Bundle\Entity\RpPlace $places)
    {
        $this->places->removeElement($places);
    }

    /**
     * Get places
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $realms;

    /**
     * Add realms
     *
     * @param \LLDC\Bundle\Entity\Realm $realms
     * @return User
     */
    public function addRealm(\LLDC\Bundle\Entity\Realm $realms)
    {
        $this->realms[] = $realms;
    
        return $this;
    }

    /**
     * Remove realms
     *
     * @param \LLDC\Bundle\Entity\Realm $realms
     */
    public function removeRealm(\LLDC\Bundle\Entity\Realm $realms)
    {
        $this->realms->removeElement($realms);
    }

    /**
     * Get realms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRealms()
    {
        return $this->realms;
    }

    /**
     * @var \DateTime
     */
    private $dateLastAction;

    /**
     * Set dateLastAction
     *
     * @param \DateTime $dateLastAction
     * @return User
     */
    public function setDateLastAction($dateLastAction)
    {
        $this->dateLastAction = $dateLastAction;

        return $this;
    }

    /**
     * Get dateLastAction
     *
     * @return \DateTime 
     */
    public function getDateLastAction()
    {
        return $this->dateLastAction;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $stories;


    /**
     * Add story
     *
     * @param \LLDC\Bundle\Entity\RpStory $story
     *
     * @return User
     */
    public function addStory(\LLDC\Bundle\Entity\RpStory $story)
    {
        $this->stories[] = $story;

        return $this;
    }

    /**
     * Remove story
     *
     * @param \LLDC\Bundle\Entity\RpStory $story
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeStory(\LLDC\Bundle\Entity\RpStory $story)
    {
        return $this->stories->removeElement($story);
    }

    /**
     * Get stories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStories()
    {
        return $this->stories;
    }
}
