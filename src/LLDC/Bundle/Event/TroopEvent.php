<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Event
 */
namespace LLDC\Bundle\Event;

use Symfony\Component\EventDispatcher\Event;
use LLDC\Bundle\Entity\Troop;

/**
 * Represents an event occurring on a troop
 */
class TroopEvent extends Event
{
    /**
     * @var Troop $troop The entity troop
     */
    private $troop;

    /**
     * @var Troop $troopOptional An optional entity troop can be given to this event
     */
    private $troopOptional;

    /**
     * Constructor, set the event properties
     *
     * @param Troop $troop
     * @param Troop $troopOptional
     */
    public function __construct(Troop $troop, Troop $troopOptional = null) {
        $this->troop = $troop;
        $this->troopOptional = $troopOptional;
        return $this;
    }

    /**
     * Return the object $troop
     *
     * @return Troop
     */
    public function getTroop()
    {
        return $this->troop;
    }

    /**
     * Set the object $troop
     *
     * @param Troop $troop
     */
    public function setTroop(Troop $troop)
    {
        $this->troop = $troop;
        return $this;
    }
    
    /**
     * Return the object $troopOptional
     *
     * @return Troop
     */
    public function getTroopOptional()
    {
        return $this->troopOptional;
    }
    /**
     * Set the object $troopOptional
     *
     * @param Troop $troopOptional
     */
    public function setTroopOptional($troopOptional)
    {
        $this->troopOptional = $troopOptional;
        return $this;
    }
}
