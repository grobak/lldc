<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace LLDC\Bundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use LLDC\Bundle\LLDCEvents;
use LLDC\Bundle\Event\TroopEvent;
use LLDC\Bundle\Entity\Troop;

/**
 * Listener responsible to manage the troops
 */
class TroopEventListener implements EventSubscriberInterface
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            LLDCEvents::WAR_TROOP_SPLIT => 'onWarTroopSplit',
            LLDCEvents::TROOP_SPLIT => 'onTroopSplit',
            LLDCEvents::TROOP_TRIGGER_BEFORE_ATTACK => 'onTroopTriggerBeforeAttack',
            LLDCEvents::TROOP_TRIGGER_BEFORE_DEFEND => 'onTroopTriggerBeforeDefend',
            LLDCEvents::TROOP_TRIGGER_AFTER_ATTACK => 'onTroopTriggerAfterAttack',
            LLDCEvents::TROOP_TRIGGER_AFTER_DEFEND => 'onTroopTriggerAfterDefend',
            LLDCEvents::TROOP_TRIGGER_BEFORE_DIRECTION_CHANGE => 'onTroopTriggerBeforeDirectionChange',
            LLDCEvents::TROOP_TRIGGER_AFTER_DIRECTION_CHANGE => 'onTroopTriggerAfterDirectionChange',
            LLDCEvents::TROOP_TRIGGER_BEFORE_END_OF_TURN => 'onTroopTriggerBeforeEndOfTurn',
            LLDCEvents::TROOP_TRIGGER_AFTER_END_OF_TURN => 'onTroopTriggerAfterEndOfTurn',
            LLDCEvents::TROOP_TRIGGER_BEFORE_INJURED => 'onTroopTriggerBeforeInjured',
            LLDCEvents::TROOP_TRIGGER_AFTER_INJURED => 'onTroopTriggerAfterInjured'
        );
    }

    public function onTroopSplit(TroopEvent $event)
    {
        /** TODO do something */
    }

    public function onWarTroopSplit(TroopEvent $event)
    {
        /** TODO do something */
    }

    private function handleTrigger(Troop $a, Troop $b, $triggerType) {
        $lldc = $this->container->getParameter('lldc');

        $service = $this->container->get('lldc.troop');

        foreach($service->getAbilities($a) as $abilityName) {

            $ability = $lldc['abilities'][$abilityName];

            if(isSet($ability['trigger-type']) && $ability['trigger-type']===$triggerType) {

                if(isSet($ability['enemy'])) { // Effects on $b
                    $stack = $b->getTmpStack();
                    foreach($ability['enemy'] as $key=>$value) {
                        $effect = isSet($stack[$key]) ? $stack[$key] : array();
                        $stack[$key] = $value;
                    }
                    $b->setTmpStack($stack);
                }

                if(isSet($ability['itself'])) { // Effects on $a
                    $stack = $a->getTmpStack();
                    foreach($ability['itself'] as $key=>$value) {
                        $effect = isSet($stack[$key]) ? $stack[$key] : array();
                        $stack[$key] = $value;
                    }
                    $a->setTmpStack($stack);
                }
            }
        }
    }

    public function onTroopTriggerBeforeAttack(TroopEvent $event) {
        $attackerTroop = $event->getTroop();
        $defenderTroop = $event->getTroopOptional();
        $this->handleTrigger($attackerTroop, $defenderTroop, 'before-attack');
    }
    public function onTroopTriggerBeforeDefend(TroopEvent $event) {
        $defenderTroop = $event->getTroop();
        $attackerTroop = $event->getTroopOptional();
        $this->handleTrigger($attackerTroop, $defenderTroop, 'before-defend');
    }
    public function onTroopTriggerAfterAttack(TroopEvent $event) {
        $attackerTroop = $event->getTroop();
        $defenderTroop = $event->getTroopOptional();
        $this->handleTrigger($attackerTroop, $defenderTroop, 'after-attack');
    }
    public function onTroopTriggerAfterDefend(TroopEvent $event) {
        $defenderTroop = $event->getTroop();
        $attackerTroop = $event->getTroopOptional();
        $this->handleTrigger($attackerTroop, $defenderTroop, 'after-defend');
    }
    public function onTroopTriggerBeforeDirectionChange(TroopEvent $event) {
        /** TODO do something */
    }
    public function onTroopTriggerAfterDirectionChange(TroopEvent $event) {
        /** TODO do something */
    }
    public function onTroopTriggerBeforeEndOfTurn(TroopEvent $event) {
        /** TODO do something */
    }
    public function onTroopTriggerAfterEndOfTurn(TroopEvent $event) {
        /** TODO do something */
    }
    public function onTroopTriggerBeforeInjured(TroopEvent $event) {
        /** TODO do something */
    }
    public function onTroopTriggerAfterInjured(TroopEvent $event) {
        /** TODO do something */
    }

}
