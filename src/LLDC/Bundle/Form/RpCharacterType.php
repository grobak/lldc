<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


namespace LLDC\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RpCharacterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('avatar', FileType::class,
                array(
                    'label' => 'realm.character.avatar.label',
                    'attr' => array('help' => 'realm.character.avatar.help'),
                    'data_class' => 'LLDC\Bundle\Entity\Image',
                    'mapped' => false,
                    'required' => false,
                ))
            ->add('name', TextType::class, array('label' => 'realm.character.name.label', 'attr' => array('help' => 'realm.character.name.help')))
            ->add('title', TextType::class, array('label' => 'realm.character.title.label', 'required' => false, 'attr' => array('help' => 'realm.character.title.help')))
            ->add('story', TextareaType::class, array('label' => 'realm.character.story.label', 'required' => false, 'attr' => array('help' => 'realm.character.story.help')))
            ->add('age', NumberType::class, array('label' => 'realm.character.age.label', 'required' => false, 'attr' => array('help' => 'realm.character.age.help')))
            ->add('firstname', TextType::class, array('label' => 'realm.character.firstname.label', 'required' => false, 'attr' => array('help' => 'realm.character.firstname.help')))
            ->add('nickname', TextType::class, array('label' => 'realm.character.nickname.label', 'required' => false, 'attr' => array('help' => 'realm.character.nickname.help')))
            ->add('height', NumberType::class, array('label' => 'realm.character.height.label', 'required' => false, 'attr' => array('help' => 'realm.character.height.help')))
            ->add('weight', NumberType::class, array('label' => 'realm.character.weight.label', 'required' => false, 'attr' => array('help' => 'realm.character.weight.help')))
            ->add('physiognomy', TextType::class, array('label' => 'realm.character.physiognomy.label', 'required' => false, 'attr' => array('help' => 'realm.character.physiognomy.help')))
            ->add('hairs', TextType::class, array('label' => 'realm.character.hairs.label', 'required' => false, 'attr' => array('help' => 'realm.character.hairs.help')))
            ->add('eyes', TextType::class, array('label' => 'realm.character.eyes.label', 'required' => false, 'attr' => array('help' => 'realm.character.eyes.help')))
            ->add('dressStyle', TextType::class, array('label' => 'realm.character.dressStyle.label', 'required' => false, 'attr' => array('help' => 'realm.character.dressStyle.help')))
            ->add('distinctiveSign', TextType::class, array('label' => 'realm.character.distinctiveSign.label', 'required' => false, 'attr' => array('help' => 'realm.character.distinctiveSign.help')))
            ->add('tastes', TextType::class, array('label' => 'realm.character.tastes.label', 'required' => false, 'attr' => array('help' => 'realm.character.tastes.help')))
            ->add('favoriteObject', TextType::class, array('label' => 'realm.character.favoriteObject.label', 'required' => false, 'attr' => array('help' => 'realm.character.favoriteObject.help')))
            ->add('companion', TextType::class, array('label' => 'realm.character.companion.label', 'required' => false, 'attr' => array('help' => 'realm.character.companion.help')))
            ->add('mainWeapon', TextType::class, array('label' => 'realm.character.mainWeapon.label', 'required' => false, 'attr' => array('help' => 'realm.character.mainWeapon.help')))
            ->add('secondaryWeapon', TextType::class, array('label' => 'realm.character.secondaryWeapon.label', 'required' => false, 'attr' => array('help' => 'realm.character.secondaryWeapon.help')))
            ->add('armor', TextType::class, array('label' => 'realm.character.armor.label', 'required' => false, 'attr' => array('help' => 'realm.character.armor.help')))
            ->add('power', TextType::class, array('label' => 'realm.character.power.label', 'required' => false, 'attr' => array('help' => 'realm.character.power.help')))
            ->add('talent', TextType::class, array('label' => 'realm.character.talent.label', 'required' => false, 'attr' => array('help' => 'realm.character.talent.help')))
            ->add('weakness', TextType::class, array('label' => 'realm.character.weakness.label', 'required' => false, 'attr' => array('help' => 'realm.character.weakness.help')))
            ->add('conveyance', TextType::class, array('label' => 'realm.character.conveyance.label', 'required' => false, 'attr' => array('help' => 'realm.character.conveyance.help')))
            ->add('inventory', TextareaType::class, array('label' => 'realm.character.inventory.label', 'required' => false, 'attr' => array('help' => 'realm.character.inventory.help')))
            //->add('defaultGame')
            ->add('defaultForum', CheckboxType::class, array('label' => 'realm.character.defaultForum.label', 'required' => false, 'attr' => array('help' => 'realm.character.defaultForum.help')))
            //->add('deleted')
            ->add('displayed', CheckboxType::class, array('label' => 'realm.character.displayed.label', 'required' => false, 'attr' => array('help' => 'realm.character.displayed.help')))
            //->add('realm') Can't be modified
            ->add('race', EntityType::class, array('label' => 'general.race', 'class' => 'LLDCBundle:Race', 'choice_label' => 'label'))
            ->add('gender', EntityType::class, array('label' => 'general.gender', 'class' => 'LLDCBundle:Gender', 'choice_label' => 'label'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LLDC\Bundle\Entity\RpCharacter'
        ));
    }
}
