<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Repository
 */
namespace LLDC\Bundle\Repository;

use Doctrine\ORM\EntityRepository;

use LLDC\Bundle\Entity\Game;
use LLDC\Bundle\Entity\Realm;

class RealmRankingRepository extends EntityRepository
{
    /**
     * Get all the realms in the given game, sorted by points ranking
     */
    public function getList(Game $game, $type) {
        $dql = '
            SELECT
                ranking
            FROM
                LLDCBundle:RealmRanking ranking
                INNER JOIN LLDCBundle:Realm realm WITH ranking.realm = realm
            WHERE
                realm.game = :game
                AND ranking.type = :type
            ORDER BY ranking.position ASC';

        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('game', $game)
            ->setParameter('type', $type);

        return $query->getResult();
    }
    /**
     * Calculates the amount of points for a realm.
     * It handles resources.
     * Units aren't managed
     *
     * @param Realm $realm
     * @param array(mixed) $resources
     */
    public function calculatePoints(Realm $realm, $resources)
    {
        $dql = '
            SELECT
                res.food * :food AS food_points,
                res.wood * :wood AS wood_points,
                res.rock * :rock AS rock_points,
                res.gold * :gold AS gold_points,
                res.madrens * :madrens AS madrens_points
            FROM
                LLDCBundle:Realm realm
                INNER JOIN LLDCBundle:RealmResource res WITH res.realm = realm
            WHERE
                realm = :realm';

        $query = $this->getEntityManager()
            ->createQuery($dql)
            ->setParameter('realm', $realm)
            ->setParameter('food', $resources['food'])
            ->setParameter('wood', $resources['wood'])
            ->setParameter('rock', $resources['rock'])
            ->setParameter('gold', $resources['gold'])
            ->setParameter('madrens', $resources['madrens']);

        return $query->getSingleResult();
    }

    public function updatePointsPosition(Game $game) {
        $query = $this->getEntityManager()->getConnection()->exec('SET @rownum = 0;');
        $query = $this->getEntityManager()->getConnection()->exec("
            UPDATE realm_ranking
            SET position = @rownum := @rownum + 1
            WHERE type = 'points' AND realm_ranking.id_realm IN (SELECT id FROM realm WHERE id_game = ".$game->getId().")
            ORDER BY value DESC");
        return true;
    }
}
