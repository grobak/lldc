/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

$(document).ready(function() {
    // Gestion des notifications
    ['alert', 'error', 'information', 'notification', 'success', 'warning'].forEach(notification);
    function notification(status, index, array) {
        if($('div').hasClass(status)) {
            var n = noty({
                text: $('.'+status).text(),
                type: status,
                layout: 'topLeft',
                closeWith: ['hover']
            });
        }
    }

    $(document).tooltip().addClass('internal_block');

    // Opening links
    $('*[link]').one('click', function() {
    	$(location).attr('href', $(this).attr('link'));
    });

    // Mobile devices menu
    $('select#menu').click(function() {
        if($(this).find("option:selected").val() != 0) {
            window.location = $(this).find("option:selected").val();
        }
    });

    // Timeout
    $('*[timeout]').each(function(index) {
        startTimeout($(this));
    });

    function startTimeout(element) {
        var until = new Date(element.attr('timeout') * 1000);
        setInterval(function() { element.html(remainingTime(until)); }, 1000);
    }

    function remainingTime(until) {
        var diff = Math.floor( until.getTime() / 1000 - timestamp);
        var display = "";
        if(diff > 0) {
            var days = Math.floor(diff/60/60/24);
            if(days > 0) {
                display = display + days + ' ' + (days>1 ? trans('general.time.days') : trans('general.time.day')) + ' ';
                diff -= days*60*60*24;
            }
            var hours = Math.floor(diff/60/60);
            if(hours > 0) {
                display = display + hours + ' ' + (hours>1 ? trans('general.time.hours') : trans('general.time.hour')) + ' ';
                diff -= hours*60*60;
            }
            var minutes = Math.floor(diff/60);
            if(minutes > 0) {
                display = display + minutes + ' ' + (minutes>1 ? trans('general.time.minutes') : trans('general.time.minute')) + ' ';
                diff -= minutes*60;
            }
            if(diff > 0) {
                var seconds = diff;
                if(display !== "") {
                    display = display + seconds + ' ' + (seconds>1 ? trans('general.time.seconds') : trans('general.time.second')) + ' ';
                }
                else {
                    display = trans('general.time.less-minute')
                }
            }
        }
        else {
            display = trans('general.time.soon');
        }

        return display;
    }

    // Search user
    $('.searchUserField').focus(function() { searchUser($(this)); }).keyup(function(event) { searchUser($(this), event); }).blur(function() { searchUserClose($(this)); });
    $('.searchUserOptionGameOnly').change(function() { searchUser($(this)); });
    $('.searchUserOptionCharacter').change(function() { searchUser($(this)); });
    $('.searchUserOptionPlace').change(function() { searchUser($(this)); });
    $('.searchUserOptionUsername').change(function() { searchUser($(this)); });
    function searchUser(input, event) {
        var fieldset = $(input).parent();
        // Called by keyup
        if(event) {
            var code = event.keyCode || event.which;
            if(code == 13) { return searchUserPerformAction(fieldset); } // Enter pressed
            else if(code == 40) { return searchUserSelectDown(fieldset); } // Down pressed
            else if(code == 38) { return searchUserSelectUp(fieldset); } // Up pressed
        }
        var string = $(fieldset).children('.searchUserField')[0].value;
        var GAME_ONLY = 1;
        if($(fieldset).children('.searchUserOptionGameOnly').children()[0]) {
            GAME_ONLY = $(fieldset).children('.searchUserOptionGameOnly').children()[0].checked ? 1 : 0;
        }
        var CHARACTER = $(fieldset).children('.searchUserOptionCharacter').children()[0].checked ? 1 : 0;
        var PLACE = $(fieldset).children('.searchUserOptionPlace').children()[0].checked ? 1 : 0;
        var USERNAME = $(fieldset).children('.searchUserOptionUsername').children()[0].checked ? 1 : 0;

        if(string.length>0) {
            $.ajax({ url: Routing.generate('json_searchUser',
                    { string: string, options: GAME_ONLY+''+CHARACTER+''+PLACE+''+USERNAME }) })
                .done(function(json) {
                    var result = '<table><tr><th>Membre</th><th>Partie</th><th>Seigneur</th><th>Royaume</th></tr>';
                    var i = 0;
                    // For each user
                    $.each(json, function(userId, data) {
                        var username = data.user.username;
                        // For each realm
                        data.realms.forEach(function(realm) {
                            result +=
                                '<tr class="result'+(i==0 ? ' selected' : '')+'" idrealm="'+realm.realm.id+'"><td>'+username+'</td><td>'+realm.game.label+'</td><td>'+realm.realm.character.name+'</td><td>'+realm.realm.place.name+'</td></tr>';
                        });
                        i++;
                    });
                    $('.searchUserResult').html('');
                    $(fieldset).children('.searchUserResult').html(result+'</table>');
                    $(fieldset).find('tr.result').on('mousedown', function() {
                        $(fieldset).find('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                        searchUserPerformAction(fieldset);
                    });
                }
            );
        }
        else {
            searchUserClose(input);
        }
    }
    function searchUserClose(input) {
        $(input).parent().children('.searchUserResult').html('');
    }
    function searchUserSelectDown(fieldset) {
        var next = $(fieldset).find('tr.selected').next();
        if(next[0]) {
            $(fieldset).find('tr.selected').removeClass('selected');
            next.addClass('selected');
        }
        return false;
    }
    function searchUserSelectUp(fieldset) {
        var prev = $(fieldset).find('tr.selected').prev();
        if(prev[0] && prev.hasClass('result')) {
            $(fieldset).find('tr.selected').removeClass('selected');
            prev.addClass('selected');
        }
        return false;
    }
    function searchUserPerformAction(fieldset) {
        var call = 'search'+$(fieldset).find('input[name=call]')[0].defaultValue;
        var result = $(fieldset).find('tr.selected');
        var username = result.children()[0].innerText;
        var gameLabel = result.children()[1].innerText;
        var characterName = result.children()[2].innerText;
        var realmName = result.children()[3].innerText;
        // We check if the function "call" is defined in window. If true, then we execute it (window[call])
        if (call in window) {
            console.debug(call + "(" + result.attr('idrealm') + ", " + username + ", " + gameLabel + ", " + characterName + ", " + realmName + ");");
            searchUserClose($(fieldset).find('.searchUserField'));
            window[call](result.attr('idrealm'), username, gameLabel, characterName, realmName);
        } else {
            console.error(call + " is undefined.");
        }
        return false;
    }
});
