<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Tests\DependencyInjection\Realm
 */
namespace LLDC\Bundle\Tests\DependencyInjection\Realm;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Util\ResourcesBean;

class ResourcesServiceTest extends WebTestCase {

    private $service;
    private $container;

    public function setUp() {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->service = $this->container->get('lldc.resources');
    }

    /**
     * This method tests the ResourcesService::take() method
     */
    public function testTake() {

        // Retrieves the first realm
        $realm = $this->container->get('doctrine')->getRepository('LLDCBundle:Realm')->findAll()[0];
        $bean = new ResourcesBean();

        /**
         * Not enough food
         */
        try {
            $bean->setFood($realm->getResource()->getFood()+1);
            $this->service->take($bean, $realm);
        }
        catch(\Exception $e) {
            $this->assertEquals($e->getMessage(), $this->container->get('translator')->trans('realm.resources.not.enough.food'));
        }
        /**
         * Enough food
         */
        $bean->setFood(1);
        $foodBefore = $realm->getResource()->getFood();
        $this->service->take($bean, $realm);
        $foodAfter = $realm->getResource()->getFood();

        $this->assertEquals(1, $foodBefore-$foodAfter);
        /**
         * Negative food
         */
        $bean->setFood(-100);
        $foodBefore = $realm->getResource()->getFood();
        $this->service->take($bean, $realm);
        $foodAfter = $realm->getResource()->getFood();

        $this->assertEquals(0, $foodBefore-$foodAfter);
        /**
         * Decimal food
         */
        $bean->setFood(0.47);
        $foodBefore = $realm->getResource()->getFood();
        $this->service->take($bean, $realm);
        $foodAfter = $realm->getResource()->getFood();

        $this->assertEquals(ceil(0.47), $foodBefore-$foodAfter);
        $bean->setFood(0);


        /**
         * Not enough wood
         */
        try {
            $bean->setWood($realm->getResource()->getWood()+1);
            $this->service->take($bean, $realm);
        }
        catch(\Exception $e) {
            $this->assertEquals($e->getMessage(), $this->container->get('translator')->trans('realm.resources.not.enough.wood'));
        }
        /**
         * Enough wood
         */
        $bean->setWood(1);
        $woodBefore = $realm->getResource()->getWood();
        $this->service->take($bean, $realm);
        $woodAfter = $realm->getResource()->getWood();

        $this->assertEquals(1, $woodBefore-$woodAfter);
        /**
         * Negative wood
         */
        $bean->setWood(-100);
        $woodBefore = $realm->getResource()->getWood();
        $this->service->take($bean, $realm);
        $woodAfter = $realm->getResource()->getWood();

        $this->assertEquals(0, $woodBefore-$woodAfter);
        /**
         * Decimal wood
         */
        $bean->setWood(0.47);
        $woodBefore = $realm->getResource()->getWood();
        $this->service->take($bean, $realm);
        $woodAfter = $realm->getResource()->getWood();

        $this->assertEquals(ceil(0.47), $woodBefore-$woodAfter);
        $bean->setWood(0);


        /**
         * Not enough gold
         */
        try {
            $bean->setGold($realm->getResource()->getGold()+1);
            $this->service->take($bean, $realm);
        }
        catch(\Exception $e) {
            $this->assertEquals($e->getMessage(), $this->container->get('translator')->trans('realm.resources.not.enough.gold'));
        }
        /**
         * Enough gold
         */
        $bean->setGold(1);
        $goldBefore = $realm->getResource()->getGold();
        $this->service->take($bean, $realm);
        $goldAfter = $realm->getResource()->getGold();

        $this->assertEquals(1, $goldBefore-$goldAfter);
        /**
         * Negative gold
         */
        $bean->setGold(-100);
        $goldBefore = $realm->getResource()->getGold();
        $this->service->take($bean, $realm);
        $goldAfter = $realm->getResource()->getGold();

        $this->assertEquals(0, $goldBefore-$goldAfter);
        /**
         * Decimal gold
         */
        $bean->setGold(0.47);
        $goldBefore = $realm->getResource()->getGold();
        $this->service->take($bean, $realm);
        $goldAfter = $realm->getResource()->getGold();

        $this->assertEquals(ceil(0.47), $goldBefore-$goldAfter);
        $bean->setGold(0);


        /**
         * Not enough rock
         */
        try {
            $bean->setRock($realm->getResource()->getRock()+1);
            $this->service->take($bean, $realm);
        }
        catch(\Exception $e) {
            $this->assertEquals($e->getMessage(), $this->container->get('translator')->trans('realm.resources.not.enough.rock'));
        }
        /**
         * Enough rock
         */
        $bean->setRock(1);
        $rockBefore = $realm->getResource()->getRock();
        $this->service->take($bean, $realm);
        $rockAfter = $realm->getResource()->getRock();

        $this->assertEquals(1, $rockBefore-$rockAfter);
        /**
         * Negative rock
         */
        $bean->setRock(-100);
        $rockBefore = $realm->getResource()->getRock();
        $this->service->take($bean, $realm);
        $rockAfter = $realm->getResource()->getRock();

        $this->assertEquals(0, $rockBefore-$rockAfter);
        /**
         * Decimal rock
         */
        $bean->setRock(0.47);
        $rockBefore = $realm->getResource()->getRock();
        $this->service->take($bean, $realm);
        $rockAfter = $realm->getResource()->getRock();

        $this->assertEquals(ceil(0.47), $rockBefore-$rockAfter);
        $bean->setRock(0);


        /**
         * Not enough madrens
         */
        try {
            $bean->setMadrens($realm->getResource()->getMadrens()+1);
            $this->service->take($bean, $realm);
        }
        catch(\Exception $e) {
            $this->assertEquals($e->getMessage(), $this->container->get('translator')->trans('realm.resources.not.enough.madrens'));
        }
        /**
         * Enough madrens
         */
        $bean->setMadrens(1);
        $madrensBefore = $realm->getResource()->getMadrens();
        $this->service->take($bean, $realm);
        $madrensAfter = $realm->getResource()->getMadrens();

        $this->assertEquals(1, $madrensBefore-$madrensAfter);
        /**
         * Negative madrens
         */
        $bean->setMadrens(-100);
        $madrensBefore = $realm->getResource()->getMadrens();
        $this->service->take($bean, $realm);
        $madrensAfter = $realm->getResource()->getMadrens();

        $this->assertEquals(0, $madrensBefore-$madrensAfter);
        /**
         * Decimal madrens
         */
        $bean->setMadrens(0.47);
        $madrensBefore = $realm->getResource()->getMadrens();
        $this->service->take($bean, $realm);
        $madrensAfter = $realm->getResource()->getMadrens();

        $this->assertEquals(ceil(0.47), $madrensBefore-$madrensAfter);
        $bean->setMadrens(0);
    }

}
