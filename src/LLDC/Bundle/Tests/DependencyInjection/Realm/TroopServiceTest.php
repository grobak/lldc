<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Tests\DependencyInjection\Realm
 */
namespace LLDC\Bundle\Tests\DependencyInjection\Realm;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use LLDC\Bundle\LLDCException;

use LLDC\Bundle\Entity\Realm;
use LLDC\Bundle\Entity\Troop;

class TroopServiceTest extends WebTestCase {

    private $service;
    private $container;

    public function setUp() {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->service = $this->container->get('lldc.troop');
    }

    public function testMerge() {
        $realm = $this->container->get('doctrine')->getRepository('LLDCBundle:Realm')->findAll()[0];

        // We create 3 troops
        $troop1 = $this->service->add($realm, 'peon', 10);
        $troop2 = $this->service->add($realm, 'peon', 10);
        $troop3 = $this->service->add($realm, 'peon', 10);

        $manager = $this->container->get('doctrine')->getManager();
        $manager->flush();

        // We try to merge troop1 into troop2
        $this->service->merge($troop1, $troop2);
        $this->assertEquals(20, $troop2->getAmount());
        $this->assertEquals(0, $troop1->getAmount());
        $this->assertNotNull($troop1->getRace());
        $this->assertTrue($manager->getUnitOfWork()->isScheduledForDelete($troop1));

        // We try to merge troop 1 into troop 3 (we want it to fail)
        $this->setExpectedException('Exception', 'The troop to merge has already been removed');
        $this->service->merge($troop1, $troop3);
    }

    public function testSplit() {
        $realm = $this->container->get('doctrine')->getRepository('LLDCBundle:Realm')->findAll()[0];

        // We create 3 troops
        $troop = $this->service->add($realm, 'peon', 100);

        $this->container->get('doctrine')->getManager()->flush();

        // We try to split the troop into two troops, changing all parameters
        $newTroop = $this->service->split($troop, 0.4, 2, 'a1');
        // Then, we check the amounts and types
        $this->assertEquals(60, $troop->getAmount());
        $this->assertEquals(80, $newTroop->getAmount());
        $this->assertEquals('peon', $troop->getType());
        $this->assertEquals('a1', $newTroop->getType());
        $this->assertEquals($troop->getRace(), $newTroop->getRace());
        $this->assertNotNull($newTroop->getRace());

        // TODO use LLDCException with a specific code, not exception's string
        // We try to split a troop by creating a new troop with an unknown unit type
        $this->setExpectedException('Exception', 'The selected type doesn\'t exist for this race.');
        $newTroop = $this->service->split($troop, 0.5, 2, 'typeThatDoesntExist');
    }

    public function testAttack() {
        $realmA = $this->container->get('doctrine')->getRepository('LLDCBundle:Realm')->findAll()[0];
        $realmB = $this->container->get('doctrine')->getRepository('LLDCBundle:Realm')->findAll()[1];

        $a = new Troop();
        $a->setRealm($realmA);
        $a->setRace($realmA->getCharacter()->getRace());
        $a->setAmount(500);
        $a->setType('a1');

        $b = new Troop();
        $b->setRealm($realmB);
        $b->setRace($realmB->getCharacter()->getRace());
        $b->setAmount(500);
        $b->setType('a1');

        $this->service->attack($a, $b, 'basic_melee');


        // We try to attack with a nonexistent ability
        $eCode = -1;
        try {
            $this->service->attack($a, $b, 'abilityWhichDoesntExist');
        } catch(LLDCException $e) {
            $eCode = $e->getCode();
        }
        $this->assertEquals(LLDCException::ABILITY_NOT_EXISTS, $eCode);

        // We try to attack a troop of the same realm. We expect an LLDCException to occur (LLDCException::TROOP_FRIENDLY_FIRE)
        $eCode = -1;
        try {
            $b->setRealm($realmA);
            $this->service->attack($a, $b, 'basic_melee');
        } catch(LLDCException $e) {
            $eCode = $e->getCode();
        }
        $this->assertEquals(LLDCException::TROOP_FRIENDLY_FIRE, $eCode);
    }

}
