<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Util
 */
namespace LLDC\Bundle\Util;

/**
 * This bean wraps the resources
 */
class ResourcesBean
{
    /**
     * @var int
     */
    private $food;
    /**
     * @var int
     */
    private $wood;
    /**
     * @var int
     */
    private $rock;
    /**
     * @var int
     */
    private $gold;
    /**
     * @var int
     */
    private $madrens;
    /**
     * @var float
     */
    private $xp;
    
    /**
     * Constructor
     * 
     * @param int $food
     * @param int $wood
     * @param int $rock
     * @param int $gold
     * @param int $madrens
     * @param float $xp
     */
    public function __construct($food = 0, $wood = 0, $rock = 0, $gold = 0, $madrens = 0, $xp = 0)
    {
        $this->setFood($food);
        $this->setWood($wood);
        $this->setRock($rock);
        $this->setGold($gold);
        $this->setMadrens($madrens);
        $this->setXp($xp);
    }
    
    /**
     * Return the food
     * 
     * @return food
     */
    public function getFood()
    {
        return $this->food;
    }
    /**
     * Set the food
     * 
     * @param int $food
     */
    public function setFood($food)
    {
        $this->food = ceil($food);
    }
    
    /**
     * Return the wood
     * 
     * @return wood
     */
    public function getWood()
    {
        return $this->wood;
    }
    /**
     * Set the wood
     * 
     * @param int $wood
     */
    public function setWood($wood)
    {
        $this->wood = ceil($wood);
    }
    
    /**
     * Return the rock
     * 
     * @return rock
     */
    public function getRock()
    {
        return $this->rock;
    }
    /**
     * Set the rock
     * 
     * @param int $rock
     */
    public function setRock($rock)
    {
        $this->rock = ceil($rock);
    }
    
    /**
     * Return the gold
     * 
     * @return gold
     */
    public function getGold()
    {
        return $this->gold;
    }
    /**
     * Set the gold
     * 
     * @param int $gold
     */
    public function setGold($gold)
    {
        $this->gold = ceil($gold);
    }
    
    /**
     * Return the madrens
     * 
     * @return madrens
     */
    public function getMadrens()
    {
        return $this->madrens;
    }
    /**
     * Set the madrens
     * 
     * @param int $madrens
     */
    public function setMadrens($madrens)
    {
        $this->madrens = ceil($madrens);
    }

    /**
     * Return the xp
     *
     * @return xp
     */
    public function getXp()
    {
        return $this->xp;
    }
    /**
     * Set the xp
     *
     * @param float $xp
     */
    public function setXp($xp)
    {
        $this->xp = $xp;
    }
}
