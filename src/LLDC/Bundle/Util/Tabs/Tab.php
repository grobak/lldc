<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Util\Tabs
 */
namespace LLDC\Bundle\Util\Tabs;

/**
 * Wraps a tab, displayed into logged_in Twig
 * @author ulti
 */
class Tab {
	private $_label;
	private $_link;
	private $_help;
	private $_active;
	
	public function __construct($label, $link, $help, $active) {
		$this->setLabel($label);
		$this->setLink($link);
		$this->setHelp($help);
		$this->setActive($active);
	}
	
	public function getLabel() {
		return $this->_label;
	}
	public function setLabel($_label) {
		$this->_label = $_label;
		return $this;
	}
	public function getLink() {
		return $this->_link;
	}
	public function setLink($_link) {
		$this->_link = $_link;
		return $this;
	}
	public function getHelp() {
		return $this->_help;
	}
	public function setHelp($_help) {
		$this->_help = $_help;
		return $this;
	}
	public function getActive() {
		return $this->_active;
	}
	public function setActive($_active) {
		$this->_active = $_active;
		return $this;
	}
	
}
