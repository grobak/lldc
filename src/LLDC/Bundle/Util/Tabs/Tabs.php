<?php
/*
 * Copyright (c) 2013-2016 LLDC dev team (see git history for details)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @package LLDC\Bundle\Util\Tabs
 */
namespace LLDC\Bundle\Util\Tabs;

use Symfony\Component\Yaml\Yaml;
use LLDC\Bundle\Exception\LLDCException;

/**
 * Tabs service
 */
class Tabs {

    private static $translator;
    private static $router;
    private static $configuration;
    private static $request;

    public function __construct($container, $requestStack) {
        self::$translator = $container->get('translator');
        self::$router = $container->get('router');
        self::$request = $requestStack->getCurrentRequest();

        // We get the fallbackLocale to use the better yml file available (and to be sure it is available ^^)
        $fallbackLocale = self::$translator->getFallbackLocales();

        // We parse the yml file for tabs
        self::$configuration = Yaml::parse(file_get_contents(
            __DIR__.'/../../Resources/translations/tabs.'.$fallbackLocale[0].'.yml'));
    }

    /**
     * Returns the tabs requested
     *
     * @param String $callFrom Example : LLDC\Bundle\Controller\LibraryController::characterAction
     * @return multitype:\LLDC\Bundle\Util\Tabs\Tab
     */
    public static function getTabs($callFrom) {
        /**
         * Retrieve the class and method which is calling, and parse the yml file to find the tabs
         *
         * Examples :
         *  - LLDC\Bundle\Controller\LibraryController::characterAction :
         *    * $controller = Library
         *    * $action = character
         *  - LLDC\Bundle\Controller\LibraryController::characterEditAction :
         *    * $controller = Library
         *    * $action = character
         */
        $split = preg_split(
            '#Controller\\\(.*)Controller::([a-z]*)(([A-Z](.*)Action)|Action)$#',
            $callFrom,
            -1,
            PREG_SPLIT_DELIM_CAPTURE
        );

        $controller = lcfirst($split[1]);
        $action = $split[2];

        if(!isSet(self::$configuration[$controller])) {
            return null;
        }

        $tabs = self::$configuration[$controller];

        /**
         * Get all $_GET parameters, used in route generation below
         */
        $parameters = array();
        foreach(self::$request->query as $key => $value) {
            $parameters[$key] = $value;
        }

        $array = array();
        foreach($tabs as $key => $value) {

            try {
                if(null === self::$router->getRouteCollection()->get($controller.ucfirst($key))) {
                    throw new LLDCException(self::$translator->trans('technical.tabs.routeGeneration'));
                }

                $route = self::$router->generate($controller.ucfirst($key), $parameters);
            }
            catch(\InvalidArgumentException $e) {
                throw new LLDCException(self::$translator->trans('technical.tabs.routeGeneration'), $e);
            }

            $array[] = new Tab(
                self::$translator->trans($controller.'.'.$key.'.label', array(), 'tabs'),
                $route,
                self::$translator->trans($controller.'.'.$key.'.help', array(), 'tabs'),
                $key === $action);
        }
        return $array;
    }
}
?>
